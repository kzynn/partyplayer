# PartyPlayer

## Development environment

Make sure to add `127.0.0.1 api.partyplayer.l` into the hosts file of your operating system. Otherwise, errors might occur. This is especially the case with **Google Chrome**.

To use the application, run the `SpringStarter` with your `IDE`. If you are using a database on your system make sure to update the credentials in the `.properties` file and create the corresponding database.
Note, that Spring is generating all tables. You only need to provide the database. The preset database name is `partyplayerapi`.
