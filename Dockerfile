FROM openjdk:8

RUN apt-get update && apt-get install -y gradle
RUN gradle -v

COPY . .
RUN ls -lat

RUN chmod +x gradlew
RUN ./gradlew assemble

RUN ls build/libs -al
ENTRYPOINT ["java","-jar", "-Dspring.profiles.active=prod", "build/libs/partyplayer-0.0.1-SNAPSHOT.jar"]
