package xyz.partyplayer.tech.api;


import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import xyz.partyplayer.entity.persistence.User;
import xyz.partyplayer.knowledge.repository.UserRepository;
import xyz.partyplayer.tech.security.JwtTokenProvider;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@TestPropertySource(locations = "classpath:application.properties")
public class AccountApiControllerAuthTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private UserRepository userRepository;

    private JwtTokenProvider jwtTokenProvider = new JwtTokenProvider();

    @MockBean
    private EmailSender emailSender;

    @Before
    public void initJwtParser() {
        jwtTokenProvider.init();
    }

    @Test
    public void contextLoads() throws Exception {
    }

    @Test
    public void loginToApiReturnsValidJwtWithSubject() throws Exception {

        User user = new User("TestUser", "TestPassword", "test@domain.tld");
        user = userRepository.save(user);

        MvcResult mvcResult = mvc.perform(post("/account/login")
                .contentType("application/json")
                .content("{\"username\": \"TestUser\", \"password\": \"TestPassword\"}"))
                .andExpect(status().isOk())
                .andReturn();

        String token = mvcResult.getResponse().getContentAsString();

        final JSONObject obj = new JSONObject(token);
        token = obj.getString("token");

        String subject = jwtTokenProvider.getSubject(token);

        assertEquals(subject, user.getId().toString());
    }
}

