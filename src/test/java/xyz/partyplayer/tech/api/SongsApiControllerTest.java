package xyz.partyplayer.tech.api;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import xyz.partyplayer.entity.persistence.Event;
import xyz.partyplayer.entity.persistence.Song;
import xyz.partyplayer.entity.persistence.User;
import xyz.partyplayer.knowledge.repository.EventRepository;
import xyz.partyplayer.knowledge.repository.SongRepository;
import xyz.partyplayer.knowledge.repository.UserRepository;
import xyz.partyplayer.tech.api.service.SongsService;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:application.properties")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@AutoConfigureMockMvc
public class SongsApiControllerTest {

    @Autowired
    private SongsService songsService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SongRepository songRepository;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private MockMvc mvc;

    @MockBean
    private EmailSender emailSender;

    @Test
    public void contextLoads() throws Exception {
    }

    @Test
    @WithMockUser(username = "TestUser", password = "TestPassword")
    public void voteSongUpShouldReturn200WhenSongVotedUp() throws Exception {
        User user = createTestUser();
        User user2 = createSecondUser();
        Event event = createEvent(user2, singletonList(user));
        Song song = createSong(user2, event);

        mvc.perform(post("/songs/" + song.getId().toString() + "/upvotes")
                .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").doesNotExist());
    }

    @Test
    @WithMockUser(username = "TestUser", password = "TestPassword")
    public void voteSongUpShouldReturn200WhenSongVotedUpAsOwner() throws Exception {
        User user = createTestUser();
        User user2 = createSecondUser();
        Event event = createEvent(user, singletonList(user));
        Song song = createSong(user2, event);

        mvc.perform(post("/songs/" + song.getId().toString() + "/upvotes")
                .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").doesNotExist());
    }

    @Test
    @WithMockUser(username = "TestUser", password = "TestPassword")
    public void voteSongUpShouldReturn404WhenSongDoesNotExist() throws Exception {
        User user = createTestUser();

        mvc.perform(post("/songs/" + UUID.randomUUID().toString() + "/upvotes")
                .contentType("application/json"))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = "TestUser", password = "TestPassword")
    public void voteSongUpShouldReturn200WhenUserIsNotMemberOfEvent() throws Exception {
        User user = createTestUser();
        User user2 = createSecondUser();
        Event event = createEvent(user2,emptyList());
        Song song = createSong(user2, event);

        mvc.perform(post("/songs/" + song.getId().toString() + "/upvotes")
                .contentType("application/json"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "TestUser", password = "TestPassword")
    public void voteSongDownShouldReturn200WhenSongVotedDown() throws Exception {
        User user = createTestUser();
        User user2 = createSecondUser();
        Event event = createEvent(user2, singletonList(user));
        Song song = createSong(user2, event);

        mvc.perform(post("/songs/" + song.getId().toString() + "/downvotes")
                .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").doesNotExist());
    }

    @Test
    @WithMockUser(username = "TestUser", password = "TestPassword")
    public void voteSongDownShouldReturn200WhenSongVotedDownAsOwner() throws Exception {
        User user = createTestUser();
        User user2 = createSecondUser();
        Event event = createEvent(user, singletonList(user));
        Song song = createSong(user2, event);

        mvc.perform(post("/songs/" + song.getId().toString() + "/downvotes")
                .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").doesNotExist());
    }

    @Test
    @WithMockUser(username = "TestUser", password = "TestPassword")
    public void voteSongDownShouldReturn404WhenSongDoesNotExist() throws Exception {
        User user = createTestUser();

        mvc.perform(post("/songs/" + UUID.randomUUID().toString() + "/downvotes")
                .contentType("application/json"))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = "TestUser", password = "TestPassword")
    public void voteSongDownShouldReturn200WhenUserIsNotMemberOfEvent() throws Exception {
        User user = createTestUser();
        User user2 = createSecondUser();
        Event event = createEvent(user2, emptyList());
        Song song = createSong(user2, event);

        mvc.perform(post("/songs/" + song.getId().toString() + "/downvotes")
                .contentType("application/json"))
                .andExpect(status().isOk());
    }

    private User createTestUser() {
        User user = new User();
        user.setUsername("TestUser");
        user.setPassword("TestPassword");
        user.setEmail("test@domain.tld");
        return userRepository.save(user);
    }

    private User createSecondUser() {
        User user2 = new User();
        user2.setUsername("TestUser2");
        user2.setPassword("TestPassword2");
        user2.setEmail("otherTest@domain.tld");
        return userRepository.save(user2);
    }

    private Event createEvent(User owner, List<User> users) {
        Event event = new Event();
        event.setOwner(owner);
        event.setDate(new Date());
        event.setName("SomeEvent");
        event.setUsers(users);
        return eventRepository.save(event);
    }

    private Song createSong(User user2, Event event) {
        Song song = new Song("SomeSong", "SomeInterpret", event, user2, "SomePlatform", "SomeLink", 0);
        song.setId(UUID.randomUUID());
        return songRepository.save(song);
    }
}
