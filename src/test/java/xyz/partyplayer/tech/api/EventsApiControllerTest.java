package xyz.partyplayer.tech.api;

import com.google.gson.Gson;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import xyz.partyplayer.entity.persistence.*;
import xyz.partyplayer.knowledge.repository.*;
import xyz.partyplayer.tech.api.model.AddSongDTO;

import java.time.ZoneOffset;
import java.util.Date;
import java.util.UUID;

import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:application.properties")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@AutoConfigureMockMvc
public class EventsApiControllerTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TmpUserRepository tmpUserRepository;


    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private SongRepository songRepository;

    @Autowired
    private VoteRepository voteRepository;

    @Autowired
    private SongHistoryRepository songHistoryRepository;

    @Autowired
    private MockMvc mvc;

    @MockBean
    private EmailSender emailSender;

    @Test
    public void contextLoads() throws Exception {
    }

    @Test
    @WithMockUser(username = "TestUser", password = "TestPassword")
    public void getAllEventsForUserReturnsListOfEventsForUser() throws Exception {
        User user = new User("TestUser", "TestPassword", "test@domain.tld");
        User user2 = new User("AnotherUser", "AnotherPass", "otherTest@domain.tld");
        user = userRepository.save(user);
        user2 = userRepository.save(user2);

        // Event as owner
        Event event1 = new Event();
        event1.setName("Event Name 1");
        event1.setDate(new Date());
        event1.setDescription("Description 1");
        event1.setOwner(user);
        event1 = eventRepository.save(event1);

        // Event as user
        Event event2 = new Event();
        event2.setName("Event Name 2");
        event2.setDate(new Date());
        event2.setDescription("Description 2");
        event2.setOwner(user2);
        event2.setUsers(singletonList(user));
        event2 = eventRepository.save(event2);

        Event event3 = new Event();
        event3.setName("Event Name 3");
        event3.setDate(new Date());
        event3.setDescription("Description 3");
        event3.setOwner(user2);
        event3 = eventRepository.save(event3);

        Event event4 = new Event();
        event4.setName("Event Name 4");
        event4.setDate(new Date());
        event4.setDescription("Description 4");
        event4.setOwner(user2);
        event4 = eventRepository.save(event4);

        mvc.perform(get("/events"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").value(event1.getId().toString()))
                .andExpect(jsonPath("$[1].id").value(event2.getId().toString()));
    }

    @Test
    @WithMockUser(username = "TestUser", password = "TestPassword")
    public void createEventEndpointReturnsCreatedEvent() throws Exception {
        User user = new User("TestUser", "TestPassword", "test@domain.tld");
        userRepository.save(user);

        mvc.perform(post("/events")
                .contentType("application/json")
                .content("{\"name\": \"My Custom Event\", \"description\": \"My Custom Description\", \"date\": \"2019-11-20T18:54:15.913Z\"}"))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.name").value("My Custom Event"))
                .andExpect(jsonPath("$.description").value("My Custom Description"))
                .andExpect(jsonPath("$.date").value("2019-11-20T18:54:15.913Z"));
    }

    @Test
    @WithMockUser(username = "TestUser", password = "TestPassword")
    public void getEventByIdReturnsEventWithId() throws Exception {
        User user = new User("TestUser", "TestPassword", "test@domain.tld");
        user = userRepository.save(user);

        // Event as owner
        Event event = new Event();
        event.setName("Event Name 1");
        event.setDate(new Date());
        event.setDescription("Description 1");
        event.setOwner(user);
        event = eventRepository.save(event);

        mvc.perform(get("/events/" + event.getId().toString()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(event.getId().toString()))
                .andExpect(jsonPath("$.relation").value("owner"));;
    }

    @Test
    @WithMockUser(username = "TestUser", password = "TestPassword")
    public void getEventByIdReturns404WhenEventDoesNotExist() throws Exception {
        User user = new User("TestUser", "TestPassword", "test@domain.tld");
        user = userRepository.save(user);

        mvc.perform(get("/events/5e74b5e0-35de-4e94-bdaa-37da413398c3"))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = "TestUser", password = "TestPassword")
    public void getEventByIdReturns404WhenUserIsNotMemberOfEvent() throws Exception {
        User user = new User("TestUser", "TestPassword", "test@domain.tld");
        user = userRepository.save(user);
        User user2 = new User("TestUser2", "TestPassword2", "otherTest@domain.tld");
        user2 = userRepository.save(user2);

        // Event as owner
        Event event = new Event();
        event.setName("Event Name 1");
        event.setDate(new Date());
        event.setDescription("Description 1");
        event.setOwner(user2);
        event = eventRepository.save(event);

        mvc.perform(get("/events/" + event.getId().toString()))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = "member", password = "TestPassword")
    public void getEventByIdReturnsRelationMemberIfRequestedAsMember() throws Exception {
        User user = new User("TestUser", "TestPassword", "test@domain.tld");
        user = userRepository.save(user);

        User user2 = new User("member", "TestPassword", "member@domain.tld");
        user2 = userRepository.save(user2);

        // Event as owner
        Event event = new Event();
        event.setName("Event Name 1");
        event.setDate(new Date());
        event.setDescription("Description 1");
        event.setOwner(user);
        event.setUsers(singletonList(user2));
        event = eventRepository.save(event);

        mvc.perform(get("/events/" + event.getId().toString()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.relation").value("member"));
    }

    @Test
    @Ignore
    public void getEventByIdReturnsRelationTmpUserIfRequestedAsTmpUser() throws Exception {
        User user = new User("TestUser", "TestPassword", "test@domain.tld");
        user = userRepository.save(user);

        // Event as owner
        Event event = new Event();
        event.setName("Event Name 1");
        event.setDate(new Date());
        event.setDescription("Description 1");
        event.setOwner(user);
        event = eventRepository.save(event);

        TmpUser user2 = new TmpUser();
        user2.setId(UUID.fromString("11-11-11-11-11"));
        user2.setEvent(event);
        user2 = tmpUserRepository.save(user2);

        mvc.perform(get("/events/" + event.getId().toString()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.relation").value("tmpUser"));
    }

    @Test
    @WithMockUser(username = "TestUser", password = "TestPassword")
    public void getEventSongsReturnsSongsOfEvent() throws Exception {
        User user = new User("TestUser", "TestPassword", "test@domain.tld");
        user = userRepository.save(user);
        User user2 = new User("TestUser2", "TestPassword", "test2@domain.tld");
        user2 = userRepository.save(user2);
        User user3 = new User("TestUser3", "TestPassword", "test3@domain.tld");
        user3 = userRepository.save(user3);
        User user4 = new User("TestUser4", "TestPassword", "test4@domain.tld");
        user4 = userRepository.save(user4);

        // Event as owner
        Event event = new Event();
        event.setName("Event Name 1");
        event.setDate(new Date());
        event.setDescription("Description 1");
        event.setOwner(user);
        event = eventRepository.save(event);

        Event event2 = new Event();
        event2.setName("Event Name 1");
        event2.setDate(new Date());
        event2.setDescription("Description 1");
        event2.setOwner(user);
        event2 = eventRepository.save(event2);

        Song song1 = new Song("song with id 1", "any musician", event, user, "", "", 0);
        songRepository.save(song1);
        songRepository.save(new Song("song with id 2", "any musician", event2, user, "", "", 0));
        songRepository.save(new Song("song with id 3", "any musician", event2, user, "", "", 0));
        Song song4 = new Song("song with id 4", "any musician", event, user, "", "", 0);
        songRepository.save(song4);

        voteRepository.save(new Vote(song4, user, true));
        voteRepository.save(new Vote(song4, user2, true));
        voteRepository.save(new Vote(song4, user3, true));
        voteRepository.save(new Vote(song4, user4, false));

        mvc.perform(get("/events/" + event.getId().toString() + "/songs"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").value(song1.getId().toString()))
                .andExpect(jsonPath("$[0].votes").value(0))
                .andExpect(jsonPath("$[1].id").value(song4.getId().toString()))
                .andExpect(jsonPath("$[1].votes").value(2));
    }

    @Test
    @WithMockUser(username = "TestUser", password = "TestPassword")
    public void getEventSongsReturns404WhenUserIsNotMemberOfEvent() throws Exception {
        User user = new User("TestUser", "TestPassword", "test@domain.tld");
        user = userRepository.save(user);
        User user2 = new User("TestUser2", "TestPassword2", "test2@domain.tld");
        user2 = userRepository.save(user2);

        // Event as owner
        Event event = new Event();
        event.setName("Event Name 1");
        event.setDate(new Date());
        event.setDescription("Description 1");
        event.setOwner(user2);
        event = eventRepository.save(event);

        mvc.perform(get("/events/" + event.getId().toString() + "/songs"))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = "TestUser", password = "TestPassword")
    public void getEventSongsReturns404WhenEventDoesNotExist() throws Exception {
        User user = new User("TestUser", "TestPassword", "test@domain.tld");
        user = userRepository.save(user);

        mvc.perform(get("/events/5e74b5e0-35de-4e94-bdaa-37da413398c3/songs"))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = "TestUser", password = "TestPassword")
    public void addSongToEventReturns404WhenEventDoesNotExist() throws Exception {
        User user = new User("TestUser", "TestPassword", "test@domain.tld");
        user = userRepository.save(user);

        AddSongDTO addSongDto = createAddSongDTO();

        mvc.perform(
                post("/events/" + UUID.randomUUID() + "/songs")
                        .contentType("application/json")
                        .content(new Gson().toJson(addSongDto)))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = "TestUser", password = "TestPassword")
    public void addSongToEventReturns404WhenUserIsNotMemberOfEvent() throws Exception {
        User user = new User("TestUser", "TestPassword", "test@domain.tld");
        user = userRepository.save(user);
        User user2 = new User("TestUser2", "TestPassword2", "test2@domain.tld");
        user2 = userRepository.save(user2);

        // Event as owner
        Event event = new Event();
        event.setName("Event Name 1");
        event.setDate(new Date());
        event.setDescription("Description 1");
        event.setOwner(user2);
        event = eventRepository.save(event);

        AddSongDTO addSongDto = createAddSongDTO();

        mvc.perform(
                post("/events/" + event.getId() + "/songs")
                        .contentType("application/json")
                        .content(new Gson().toJson(addSongDto)))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = "TestUser", password = "TestPassword")
    public void addSongToEventCreatesSongAsOwner() throws Exception {
        User user = new User("TestUser", "TestPassword", "test@domain.tld");
        user = userRepository.save(user);

        // Event as owner
        Event event = new Event();
        event.setName("Event Name 1");
        event.setDate(new Date());
        event.setDescription("Description 1");
        event.setOwner(user);
        event = eventRepository.save(event);

        AddSongDTO addSongDto = createAddSongDTO();

        assertSongAdded(event, addSongDto);
    }

    @Test
    @WithMockUser(username = "TestUser", password = "TestPassword")
    public void addSongToEventCreatesSongAsMember() throws Exception {
        User user = new User("TestUser", "TestPassword", "test@domain.tld");
        user = userRepository.save(user);
        User user2 = new User("TestUser2", "TestPassword2", "test2@domain.tld");
        user2 = userRepository.save(user2);

        // Event as owner
        Event event = new Event();
        event.setName("Event Name 1");
        event.setDate(new Date());
        event.setDescription("Description 1");
        event.setOwner(user2);
        event.setUsers(singletonList(user));
        event = eventRepository.save(event);

        AddSongDTO addSongDto = createAddSongDTO();

        assertSongAdded(event, addSongDto);
    }

    @Test
    @WithMockUser(username = "TestUser", password = "TestPassword")
    public void getPlayedSongsReturns404WhenEventDoesNotExist() throws Exception {
        User user = new User("TestUser", "TestPassword", "test@domain.tld");
        user = userRepository.save(user);

        mvc.perform(
                get("/events/" + UUID.randomUUID().toString() + "/history"))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = "TestUser", password = "TestPassword")
    public void getPlayedSongsReturns404WhenUserIsNotMemberOfEvent() throws Exception {
        User user = new User("TestUser", "TestPassword", "test@domain.tld");
        user = userRepository.save(user);
        User user2 = new User("TestUser2", "TestPassword2", "test2@domain.tld");
        user2 = userRepository.save(user2);

        // Event as owner
        Event event = new Event();
        event.setName("Event Name 1");
        event.setDate(new Date());
        event.setDescription("Description 1");
        event.setOwner(user2);
        event = eventRepository.save(event);

        mvc.perform(
                get("/events/" + event.getId().toString() + "/history"))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = "TestUser", password = "TestPassword")
    public void getPlayedSongsReturnsPlayedSongs() throws Exception {
        User user = new User("TestUser", "TestPassword", "test@domain.tld");
        user = userRepository.save(user);

        // Event as owner
        Event event = new Event();
        event.setName("Event Name 1");
        event.setDate(new Date());
        event.setDescription("Description 1");
        event.setOwner(user);
        event = eventRepository.save(event);

        SongHistory songHistory1 = new SongHistory();
        songHistory1.setEvent(event);
        songHistory1.setName("Song3");
        songHistory1.setInterpret("Interpret3");
        songHistory1.setDuration(3);
        songHistory1.setPlatform("platform3");
        songHistory1.setPlayLink("Link3");
        songHistory1.setSuggestedAt(new Date());
        songHistory1.setVotes(3);
        songHistory1 = songHistoryRepository.save(songHistory1);

        mvc.perform(
                get("/events/" + event.getId().toString() + "/history"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").exists())
                .andExpect(jsonPath("$[0].title").value(songHistory1.getName()))
                .andExpect(jsonPath("$[0].artist").value(songHistory1.getInterpret()))
                .andExpect(jsonPath("$[0].duration").value(songHistory1.getDuration()))
                .andExpect(jsonPath("$[0].platform").value(songHistory1.getPlatform()))
                .andExpect(jsonPath("$[0].votes").value(songHistory1.getVotes()))
                .andExpect(jsonPath("$[0].playlink").value(songHistory1.getPlayLink()))
                .andExpect(jsonPath("$[0].suggestedAt").value(songHistory1.getSuggestedAt().toInstant()
                        .atOffset(ZoneOffset.UTC).toString()))
                .andExpect(jsonPath("$[0].playedAt").value(songHistory1.getPlayedAt().toInstant()
                        .atOffset(ZoneOffset.UTC).toString()));
    }

    private void assertSongAdded(Event event, AddSongDTO addSongDto) throws Exception {
        mvc.perform(
                post("/events/" + event.getId() + "/songs")
                        .contentType("application/json")
                        .content(new Gson().toJson(addSongDto)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.title").value(addSongDto.getTitle()))
                .andExpect(jsonPath("$.artist").value(addSongDto.getArtist()))
                .andExpect(jsonPath("$.duration").value(addSongDto.getDuration()))
                .andExpect(jsonPath("$.platform").value(addSongDto.getPlatform()))
                .andExpect(jsonPath("$.votes").value(0))
                .andExpect(jsonPath("$.playlink").value(addSongDto.getPlaylink()))
                .andExpect(jsonPath("$.suggestedAt").exists());
    }

    private AddSongDTO createAddSongDTO() {
        AddSongDTO addSongDto = new AddSongDTO();
        addSongDto.setTitle("SongTitle");
        addSongDto.setArtist("SongArtist");
        addSongDto.setDuration(90);
        addSongDto.setPlatform("SongPlatform");
        addSongDto.setPlaylink("SongLink");
        return addSongDto;
    }
}
