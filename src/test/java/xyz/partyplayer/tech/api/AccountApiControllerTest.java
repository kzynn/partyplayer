package xyz.partyplayer.tech.api;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import xyz.partyplayer.entity.persistence.User;
import xyz.partyplayer.knowledge.repository.UserRepository;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@TestPropertySource(locations = "classpath:application.properties")
public class AccountApiControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private UserRepository userRepository;

    @MockBean
    private EmailSender emailSender;

    @Test
    public void contextLoads() throws Exception {
    }

    @Test
    public void registerUserReturnsUserWithIdAndEmptyErrorList() throws Exception {
        mvc.perform(post("/account/register")
                .contentType("application/json")
                .content("{\"username\": \"TestUser\", \"email\": \"test@domain.tld\", \"password\": \"password\"}"))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.user.id").exists())
                .andExpect(jsonPath("$.error").isEmpty());
    }

    @Test
    public void registerUserWithAlreadyTakenUsernameReturnsEmptyUserAndUsernameError() throws Exception {
        User user = new User();
        user.setUsername("TakenUsername");
        user.setEmail("taken@domain.tld");
        user.setPassword("password");
        userRepository.save(user);

        mvc.perform(post("/account/register")
                .contentType("application/json")
                .content("{\"username\": \"TakenUsername\", \"email\": \"test@domain.tld\", \"password\": \"password\"}"))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.user").isEmpty())
                .andExpect(jsonPath("$.error", Matchers.containsInAnyOrder("username")));
    }

    @Test
    public void registerUserWithAlreadyTakenEmailReturnsEmptyUserAndEmailError() throws Exception {
        User user = new User();
        user.setUsername("TakenUsername");
        user.setEmail("taken@domain.tld");
        user.setPassword("password");
        userRepository.save(user);

        mvc.perform(post("/account/register")
                .contentType("application/json")
                .content("{\"username\": \"TestUser\", \"email\": \"taken@domain.tld\", \"password\": \"password\"}"))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.user").isEmpty())
                .andExpect(jsonPath("$.error", Matchers.containsInAnyOrder("email")));
    }

    @Test
    public void registerUserWithAlreadyTakenUsernameAndEmailReturnsEmptyUserAndUsernameAndEmailError() throws Exception {
        User user = new User();
        user.setUsername("TakenUsername");
        user.setEmail("taken@domain.tld");
        user.setPassword("password");
        userRepository.save(user);

        mvc.perform(post("/account/register")
                .contentType("application/json")
                .content("{\"username\": \"TakenUsername\", \"email\": \"taken@domain.tld\", \"password\": \"password\"}"))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.user").isEmpty())
                .andExpect(jsonPath("$.error", Matchers.containsInAnyOrder("email", "username")));
    }

    @Test
    @WithMockUser(username = "TestUser", password = "TestPassword")
    public void updateUsernameUpdatesUsername() throws Exception {
        User user = new User("TestUser", "TestPassword", "test@domain.tld");
        user = userRepository.save(user);

        mvc.perform(post("/account/username")
                .contentType("application/json")
                .content("{\"username\": \"useruser\"}"))
                .andExpect(jsonPath("$.username").exists())
                .andExpect(jsonPath("$.username").value("useruser"));
    }

}
