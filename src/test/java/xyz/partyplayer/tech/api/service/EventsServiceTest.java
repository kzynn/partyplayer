package xyz.partyplayer.tech.api.service;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import xyz.partyplayer.entity.persistence.Event;
import xyz.partyplayer.entity.persistence.Song;
import xyz.partyplayer.entity.persistence.User;
import xyz.partyplayer.knowledge.repository.EventRepository;
import xyz.partyplayer.knowledge.repository.SongRepository;
import xyz.partyplayer.knowledge.repository.UserRepository;
import xyz.partyplayer.tech.api.EmailSender;

import java.util.*;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@TestPropertySource(locations = "classpath:application.properties")
@EnableTransactionManagement
public class EventsServiceTest {

    @Autowired
    private EventsService eventsService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private SongRepository songRepository;

    @MockBean
    private EmailSender emailSender;

    @Test
    public void loadsContext() throws Exception {
    }

    @Test
    public void eventIsSavedWhenCreated() throws Exception {
        User user = userRepository.save(new User("TestUser", "TestPassword", "test@domain.tld"));
        Date date = getDate();

        Event createdEvent = eventsService.create("My really custom event", "My description for my really custom event", date, user);

        Optional<Event> event = eventRepository.findById(createdEvent.getId());
        assertThat(event)
                .isPresent()
                .get()
                .isEqualToIgnoringGivenFields(createdEvent, "owner", "users", "songs", "playedSongs", "createdAt", "tmpUsers");

        assertThat(event.get().getOwner())
                .isEqualToIgnoringGivenFields(user, "roles", "events", "registeredAt");
    }

    @Test
    public void getEventByIdReturnsEventWithIdWhenUserIsOwner() throws Exception {
        User user = userRepository.save(new User("TestUser", "TestPassword", "test@domain.tld"));
        Event event = new Event();
        event.setName("My custom event");
        event.setDate(getDate());
        event.setOwner(user);
        event = eventRepository.save(event);

        Optional<Event> retEvent = eventsService.getByIdAndUser(event.getId(), user);
        assertThat(retEvent)
                .isPresent()
                .get()
                .isEqualToIgnoringGivenFields(event, "owner", "users", "songs", "playedSongs", "createdAt", "tmpUsers");
    }

    @Test
    public void getEventByIdReturnsEventWithIdWhenUserIsMember() throws Exception {
        User user = userRepository.save(new User("TestUser", "TestPassword", "test@domain.tld"));
        User user2 = userRepository.save(new User("TestUser2", "TestPassword2", "otherTest@domain.tld"));
        Event event = new Event();
        event.setName("My custom event");
        event.setDate(getDate());
        event.setOwner(user2);
        event.setUsers(singletonList(user));
        event = eventRepository.save(event);

        Optional<Event> retEvent = eventsService.getByIdAndUser(event.getId(), user);
        assertThat(retEvent)
                .isPresent()
                .get()
                .isEqualToIgnoringGivenFields(event, "owner", "users", "songs", "playedSongs", "createdAt", "tmpUsers");
    }

    @Test
    public void getEventByIdReturnsEmptyOptionalWhenUserIsNotMemberOrOwnerOfEvent() throws Exception {
        User user = userRepository.save(new User("TestUser", "TestPassword", "test@domain.tld"));
        User user2 = userRepository.save(new User("TestUser2", "TestPassword2", "otherTest@domain.tld"));

        Event event = new Event();
        event.setName("My custom event");
        event.setDate(getDate());
        event.setOwner(user2);
        event = eventRepository.save(event);

        Optional<Event> retEvent = eventsService.getByIdAndUser(event.getId(), user);
        assertThat(retEvent).isEmpty();
    }

    @Test
    public void getEventByIdReturnsEmptyOptionalWhenEventDoesNotExist() throws Exception {
        User user = userRepository.save(new User("TestUser", "TestPassword", "test@domain.tld"));

        Optional<Event> retEvent = eventsService.getByIdAndUser(UUID.randomUUID(), user);
        assertThat(retEvent).isEmpty();
    }

    @Test
    public void deleteEventDeletesEvent() {
        Event event = new Event();
        event.setName("testevent");
        event.setDate(getDate());
        User user = userRepository.save(new User("TestUser", "TestPassword", "test@domain.tld"));
        event.setOwner(user);
        event = eventRepository.save(event);

        eventsService.deleteById(event.getId());

        Optional<Event> retEvent = eventsService.getByIdAndUser(event.getId(), user);
        assertThat(retEvent).isEmpty();
    }

    @Test
    public void inviteUserShouldAddUserAsMemberOfEvent() throws Exception {
        User owner = new User("Owner", "123", "owner@domain.tld");
        owner = userRepository.save(owner);

        User subjectUser = new User("TestUser", "TestPassword", "test@domain.tld");
        subjectUser = userRepository.save(subjectUser);

        Event event = new Event();
        event.setName("My custom event");
        event.setDate(getDate());
        event.setOwner(owner);
        event.setUsers(new ArrayList<>());
        event = eventRepository.save(event);

        this.eventsService.addUserToEvent(event, subjectUser.getUsername(), owner);

        Optional<Event> eventFromDatabase = eventRepository.findById(event.getId());

        assertThat(eventFromDatabase)
                .isPresent();
        assertThat(eventFromDatabase.get().getUsers())
                .hasSize(1);
        assertThat(eventFromDatabase.get().getUsers().get(0).getId()).isEqualTo(subjectUser.getId());
    }

    @Test
    @Ignore
    public void inviteUserAlreadyInvitedShouldNotAddHimAgain() throws Exception {
        User owner = new User("Owner", "123", "owner@domain.tld");
        owner = userRepository.save(owner);

        User subjectUser = new User("TestUser", "TestPassword", "test@domain.tld");
        subjectUser = userRepository.save(subjectUser);

        Event event = new Event();
        event.setName("My custom event");
        event.setDate(getDate());
        event.setOwner(owner);
        event.setUsers(singletonList(subjectUser));
        event = eventRepository.save(event);

        this.eventsService.addUserToEvent(event, subjectUser.getUsername(), owner);

        Optional<Event> eventFromDatabase = eventRepository.findById(event.getId());

        assertThat(eventFromDatabase)
                .isPresent();
        assertThat(eventFromDatabase.get().getUsers())
                .hasSize(1);
        assertThat(eventFromDatabase.get().getUsers())
                .contains(subjectUser);
    }

    @Test
    public void inviteUserWhenUserToAddDoesNotExistsDoNothing() {
        User owner = new User("Owner", "123", "owner@domain.tld");
        owner = userRepository.save(owner);

        Event event = new Event();
        event.setName("My custom event");
        event.setDate(getDate());
        event.setOwner(owner);
        event = eventRepository.save(event);

        this.eventsService.addUserToEvent(event, "anyNotExistingUserName", owner);

        assertThat(this.eventsService.getById(event.getId()).get().getUsers().isEmpty()).isEqualTo(true);
    }

    @Test
    @Ignore
    public void inviteUserShouldShouldThrowErrorIfEventNotFound() {
        User owner = new User("Owner", "123", "owner@domain.tld");
        User savedOwner = userRepository.save(owner);
        User subjectUser = new User("TestUser", "TestPassword", "test@domain.tld");
        userRepository.save(subjectUser);

        Event event = new Event();
        event.setName("My custom event");
        event.setDate(getDate());
        event.setOwner(savedOwner);
        eventRepository.save(event);

        try {
            this.eventsService.addUserToEvent(new Event(), subjectUser.getUsername(), savedOwner);
        } catch (Exception e) {
            assertThat(e).isEqualTo(new Exception("Event not found"));
        }
    }

    @Test
    public void inviteUserShouldShouldThrowErrorIfNotTheOwnerTriesToAdd() {
        User owner = new User("Owner", "123", "owner@domain.tld");
        User savedOwner = userRepository.save(owner);
        User subjectUser = new User("TestUser", "TestPassword", "test@domain.tld");
        userRepository.save(subjectUser);

        Event event = new Event();
        event.setName("My custom event");
        event.setDate(getDate());
        event.setOwner(savedOwner);
        event.setUsers(new ArrayList<>());
        event = eventRepository.save(event);

        User randomUser = new User();
        randomUser.setId(UUID.randomUUID());

        try {
            this.eventsService.addUserToEvent(event, subjectUser.getUsername(), randomUser);
        } catch (Exception e) {
            assertThat(e).isEqualTo(new Exception("Unauthorized"));
        }
    }

    private Date getDate() {
        // Only get Date without time so setting hours, minutes, seconds and millis to zero
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }
}
