package xyz.partyplayer.tech.api.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import xyz.partyplayer.entity.persistence.User;
import xyz.partyplayer.entity.persistence.SpotifyToken;
import xyz.partyplayer.entity.persistence.Event;
import xyz.partyplayer.entity.persistence.Song;
import xyz.partyplayer.entity.persistence.Vote;
import xyz.partyplayer.knowledge.repository.SpotifyTokenRepository;
import xyz.partyplayer.knowledge.repository.EventRepository;
import xyz.partyplayer.knowledge.repository.SongRepository;
import xyz.partyplayer.knowledge.repository.VoteRepository;
import xyz.partyplayer.knowledge.repository.UserRepository;
import xyz.partyplayer.tech.api.EmailSender;


import java.util.Collections;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@TestPropertySource(locations = "classpath:application.properties")
public class UserServiceDeleteTest {

    @Autowired
    private UserRepository repo;

    @Autowired
    private UserService userService;

    @Autowired
    private SpotifyTokenRepository spotifyTokenRepository;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private SongRepository songRepository;

    @Autowired
    private VoteRepository voteRepository;

    @MockBean
    private EmailSender emailSender;

    @Test
    @WithMockUser(username = "TestUser", password = "TestPassword")
    public void deleteUser() throws Exception {
        User user = new User("TestUser", "TestPassword", "test@domain.tld");
        user  = repo.save(user);
        userService.delete();

        assertThat(user.getEvents()).isEqualTo(null);
        assertThat(repo.count()).isEqualTo(1L);
        assertThat(repo.findAll().iterator().next().getUsername()).isEqualTo("Deleted User " + user.getId().toString());

    }

    @Test
    @WithMockUser(username = "TestUser", password = "TestPassword")
    public void deleteUserWithSpotifyToken() throws Exception {
        User user = new User("TestUser", "TestPassword", "test@domain.tld");
        user  = repo.save(user);


        SpotifyToken token = new SpotifyToken();
        token.setAccessToken("access Token");
        token.setAccessTokenExpire(new Date());
        token.setRefreshToken("refresh Token");
        token.setUser_id(user.getId());
        token.setUser(user);

        spotifyTokenRepository.save(token);

        userService.delete();

        assertThat(spotifyTokenRepository.count()).isEqualTo(0L);
        assertThat(repo.count()).isEqualTo(1L);
        assertThat(repo.findAll().iterator().next().getUsername()).isEqualTo("Deleted User " + user.getId().toString());
    }

    @Test
    @WithMockUser(username = "TestUser", password = "TestPassword")
    public void deleteUserWithEvent_eventOwnerToDefaultUser() throws Exception {
        User user = new User("TestUser", "TestPassword", "test@domain.tld");
        user  = repo.save(user);

        Event event = new Event();
        event.setName("testevent");
        event.setDate(new Date());
        event.setOwner(user);
        event = eventRepository.save(event);

        userService.delete();

        assertThat(eventRepository.count()).isEqualTo(1L);
        assertThat(repo.count()).isEqualTo(1L);
        assertThat(repo.findAll().iterator().next().getUsername().startsWith("Deleted User")).isTrue();
    }


    @Test
    @WithMockUser(username = "TestUser", password = "TestPassword")
    public void deleteUserWithEvent_eventMemberOfEvent() throws Exception {
        User user = new User("TestUser", "TestPassword", "test@domain.tld");
        user  = repo.save(user);

        User owner = new User("TestOwner", "TestPassword", "testOwner@domain.tld");
        owner  = repo.save(owner);

        Event event = new Event();
        event.setName("testevent");
        event.setDate(new Date());
        event.setOwner(owner);
        event.setUsers(Collections.singletonList(user));
        event = eventRepository.save(event);

        userService.delete();

        assertThat(repo.count()).isEqualTo(2L);
        assertThat(eventRepository.count()).isEqualTo(1L);
        assertThat(eventRepository.findAll().iterator().next().getUsers().size()).isEqualTo(1);
        assertThat(eventRepository.findAll().iterator().next().getUsers().get(0).getUsername().startsWith("Deleted User")).isTrue();

    }

    @Test
    @WithMockUser(username = "TestUser", password = "TestPassword")
    public void deleteUserWithEvent_IfHeSuggestAnSong() throws Exception {
        User user = new User("TestUser", "TestPassword", "test@domain.tld");
        user  = repo.save(user);

        User owner = new User("TestOwner", "TestPassword", "testOwner@domain.tld");
        owner  = repo.save(owner);


        Event event = new Event();
        event.setName("testevent");
        event.setDate(new Date());
        event.setOwner(owner);
        event = eventRepository.save(event);

        Song s = new Song();
        s.setName("Song name");
        s.setPlatform("any platform");
        s.setPlayLink("any playlink");
        s.setEvent(event);
        s.setSuggestedBy(user);
        songRepository.save(s);

        userService.delete();

        assertThat(repo.count()).isEqualTo(2L);
        assertThat(eventRepository.count()).isEqualTo(1L);
        assertThat(songRepository.count()).isEqualTo(1L);
        assertThat(songRepository.findAll().iterator().next().getSuggestedBy().getUsername().startsWith("Deleted User")).isTrue();

    }

    @Test
    @WithMockUser(username = "TestUser", password = "TestPassword")
    public void deleteUserWithEvent_IfHeVoteAnSuggestedSong() throws Exception {
        User user = new User("TestUser", "TestPassword", "test@domain.tld");
        user  = repo.save(user);

        User owner = new User("TestOwner", "TestPassword", "testOwner@domain.tld");
        owner  = repo.save(owner);


        Event event = new Event();
        event.setName("testevent");
        event.setDate(new Date());
        event.setOwner(owner);
        event = eventRepository.save(event);

        Song s = new Song();
        s.setName("Song name");
        s.setPlatform("any platform");
        s.setPlayLink("any playlink");
        s.setEvent(event);
        s.setSuggestedBy(owner);
        s = songRepository.save(s);

        Vote v = new Vote(s, user, true);
        v = voteRepository.save(v);

        userService.delete();

        assertThat(repo.count()).isEqualTo(2L);
        assertThat(eventRepository.count()).isEqualTo(1L);
        assertThat(eventRepository.findAll().iterator().next().getSongs().size()).isEqualTo(1);
        assertThat(songRepository.count()).isEqualTo(1L);
        assertThat(voteRepository.count()).isEqualTo(1L);
        assertThat(voteRepository.findAll().iterator().next().getUser().getUsername().startsWith("Deleted User")).isTrue();

    }
}
