package xyz.partyplayer.tech.api.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import xyz.partyplayer.entity.persistence.Role;
import xyz.partyplayer.entity.persistence.User;
import xyz.partyplayer.knowledge.repository.UserRepository;
import xyz.partyplayer.tech.api.EmailSender;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@TestPropertySource(locations = "classpath:application.properties")
public class UserServiceTest {

    @Autowired
    private UserService userService;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private EmailSender emailSender;

    @Test
    public void contextLoads() {
    }

    @Test
    @WithMockUser(username = "TestUser", password = "TestPassword")
    public void getAuthenticatedUserReturnsAuthenticatedUser() throws Exception {
        User user = new User("TestUser", "TestPassword", "test@domain.tld");
        user.setRoles(new ArrayList<Role>());
        when(userRepository.findByUsername("TestUser")).thenReturn(user);
        assertThat(userService.getAuthenticatedUser()).isEqualTo(user);
    }
}
