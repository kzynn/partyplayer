package xyz.partyplayer.tech.api.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import xyz.partyplayer.entity.persistence.Event;
import xyz.partyplayer.entity.persistence.User;
import xyz.partyplayer.knowledge.repository.EventRepository;
import xyz.partyplayer.knowledge.repository.UserRepository;
import xyz.partyplayer.tech.api.EmailSender;
import xyz.partyplayer.tech.api.model.SpotifyWebToken;
import xyz.partyplayer.usecases.IGetCredentials;
import xyz.partyplayer.usecases.IPlaySong;

import java.lang.reflect.Field;
import java.util.Calendar;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@TestPropertySource(locations = "classpath:application.properties")
@EnableTransactionManagement
public class SpotifyServiceTest {

    @Autowired
    private SpotifyService spotifyService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EventRepository eventRepository;

    @MockBean
    private EmailSender emailSender;

    @Test
    public void getPlayToken() throws Exception {
        Event event = new Event();
        event.setName("testevent");
        event.setDate(getDate());
        User user = userRepository.save(new User("TestUser", "TestPassword", "test@domain.tld"));
        event.setOwner(user);
        event = eventRepository.save(event);

        IGetCredentials cred = user1 -> "token";

        Field credentials = SpotifyService.class.getDeclaredField("credentials");
        credentials.setAccessible(true);
        credentials.set(spotifyService, cred);

        SpotifyWebToken playToken = spotifyService.getPlayToken(user.getId());

        assertThat(playToken.getSpotifyWebToken()).isEqualTo("token");
    }

    @Test
    @WithMockUser(username = "TestUser", password = "TestPassword")
    public void playSong() throws Exception {
        User user = new User();
        user.setUsername("TestUser");
        user.setPassword("TestPassword");
        user.setEmail("test@domain.tld");
        userRepository.save(user);

        final boolean[] wrongArgument = {false};

        IPlaySong play = (user1, device, trackId) -> {
            if (user1 == null || !user1.getUsername().equals("TestUser")){
                wrongArgument[0] = true;
            }
            if (!"device".equals(device)){
                wrongArgument[0] = true;
            }
            if (!"trackId".equals(trackId)){
                wrongArgument[0] = true;
            }
        };

        Field credentials = SpotifyService.class.getDeclaredField("play");
        credentials.setAccessible(true);
        credentials.set(spotifyService, play);

        spotifyService.playSong("trackId", "device");

        assertThat(wrongArgument[0]).isFalse();

    }

    private Date getDate() {
        // Only get Date without time so setting hours, minutes, seconds and millis to zero
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

}
