package xyz.partyplayer.tech.api.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import xyz.partyplayer.entity.persistence.*;
import xyz.partyplayer.knowledge.repository.*;
import xyz.partyplayer.entity.persistence.Event;
import xyz.partyplayer.entity.persistence.Song;
import xyz.partyplayer.entity.persistence.SongHistory;
import xyz.partyplayer.entity.persistence.User;
import xyz.partyplayer.entity.persistence.Vote;
import xyz.partyplayer.knowledge.repository.EventRepository;
import xyz.partyplayer.knowledge.repository.SongHistoryRepository;
import xyz.partyplayer.knowledge.repository.SongRepository;
import xyz.partyplayer.knowledge.repository.UserRepository;
import xyz.partyplayer.knowledge.repository.VoteRepository;
import xyz.partyplayer.tech.api.EmailSender;
import xyz.partyplayer.tech.api.model.AddSongDTO;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@TestPropertySource(locations = "classpath:application.properties")
public class SongsServiceTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SongRepository songRepository;

    @Autowired
    private SongHistoryRepository songHistoryRepository;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private VoteRepository voteRepository;

    @Autowired
    private SongsService songsService;

    @MockBean
    private EmailSender emailSender;

    @Test
    public void loadsContext() throws Exception {
    }

    @Test
    public void getByIdReturnsNullWhenInvalidUUIDPassed() throws Exception {
        assertThat(songsService.getById(UUID.randomUUID())).isEqualTo(null);
    }

    @Test
    public void getByIdReturnsNullWhenListDoesNotExist() throws Exception {
        assertThat(songsService.getById(UUID.randomUUID())).isEqualTo(null);
    }

    @Test
    public void getByIdReturnsSongWithGivenId() throws Exception {
        User user = getMockUser();
        Event event = getTestEvent(user);

        Song song = getTestSong(user, event);

        Song ret = songsService.getById(song.getId());
        assertThat(ret).isEqualToIgnoringGivenFields(song, "suggestedAt", "event", "suggestedBy", "votes");
    }

    @Test
    @WithMockUser(username = "TestUser", password = "TestPassword")
    public void voteSongVotesSongUp() throws Exception {
        User user = getMockUser();
        User user2 = getTestUser();

        Event event = new Event();
        event.setOwner(user2);
        event.setDate(new Date());
        event.setName("SomeEvent");
        event.setUsers(singletonList(user));
        event = eventRepository.save(event);

        Song song = getTestSong(user2, event);

        songsService.voteSong(song, user, true);

        assertThat(voteRepository.findAll()).containsOnly(new Vote(song, user, true));
    }

    @Test
    @WithMockUser(username = "TestUser", password = "TestPassword")
    public void voteSongVotesSongDown() throws Exception {
        User user = getMockUser();
        User user2 = getTestUser();

        Event event = new Event();
        event.setOwner(user2);
        event.setDate(new Date());
        event.setName("SomeEvent");
        event.setUsers(singletonList(user));
        event = eventRepository.save(event);

        Song song = getTestSong(user2, event);

        songsService.voteSong(song, user, false);

        assertThat(voteRepository.findAll()).hasSize(1).containsOnly(new Vote(song, user, false));
    }

    @Test
    @WithMockUser(username = "TestUser", password = "TestPassword")
    public void voteSongVotesSongDownAsOwner() throws Exception {
        User user = getMockUser();
        User user2 = getTestUser();

        Event event = new Event();
        event.setOwner(user);
        event.setDate(new Date());
        event.setName("SomeEvent");
        event.setUsers(singletonList(user2));
        event = eventRepository.save(event);

        Song song = getTestSong(user2, event);

        songsService.voteSong(song, user, false);

        assertThat(voteRepository.findAll()).hasSize(1).containsOnly(new Vote(song, user, false));
    }

    @Test
    @WithMockUser(username = "TestUser", password = "TestPassword")
    public void voteSongShouldDoNothingWhenUserIsNotMemberOfEvent() throws Exception {
        User user = getMockUser();
        User user2 = getTestUser();
        Event event = getTestEvent(user2);

        Song song = new Song("SomeSong", "SomeInterpret", event, user2, "SomePlatform", "SomeLink", 0);
        song = songRepository.save(song);

        songsService.voteSong(song, user, true);

        assertThat(songRepository.findById(song.getId()).get().getVotes()).hasSize(0);
    }

    @Test
    @WithMockUser(username = "TestUser", password = "TestPassword")
    public void shouldNotAllowDoubleVoteToTheSameSongWithDownDown() throws Exception {
        User user = getMockUser();
        User user2 = getTestUser();

        Event event = new Event();
        event.setOwner(user2);
        event.setDate(new Date());
        event.setName("SomeEvent");
        event.setUsers(singletonList(user));
        event = eventRepository.save(event);

        Song song = getTestSong(user2, event);

        songsService.voteSong(song, user, false);
        songsService.voteSong(song, user, false);

        assertThat(voteRepository.findAll()).hasSize(1).containsOnly(new Vote(song, user, false));
    }

    @Test
    @WithMockUser(username = "TestUser", password = "TestPassword")
    public void shouldNotAllowDoubleVoteToTheSameSongWithDownUp() throws Exception {
        User user = getMockUser();
        User user2 = getTestUser();

        Event event = new Event();
        event.setOwner(user2);
        event.setDate(new Date());
        event.setName("SomeEvent");
        event.setUsers(singletonList(user));
        event = eventRepository.save(event);

        Song song = getTestSong(user2, event);

        songsService.voteSong(song, user, false);
        songsService.voteSong(song, user, true);

        assertThat(voteRepository.findAll()).hasSize(1).containsOnly(new Vote(song, user, false));
    }

    @Test
    @WithMockUser(username = "TestUser", password = "TestPassword")
    public void shouldNotAllowDoubleVoteToTheSameSongWithUpUp() throws Exception {
        User user = getMockUser();
        User user2 = getTestUser();

        Event event = new Event();
        event.setOwner(user2);
        event.setDate(new Date());
        event.setName("SomeEvent");
        event.setUsers(singletonList(user));
        event = eventRepository.save(event);

        Song song = getTestSong(user2, event);

        songsService.voteSong(song, user, true);
        songsService.voteSong(song, user, true);

        assertThat(voteRepository.findAll()).hasSize(1).containsOnly(new Vote(song, user, true));
    }

    @Test
    @WithMockUser(username = "TestUser", password = "TestPassword")
    public void shouldNotAllowDoubleVoteToTheSameSongWithUpDown() throws Exception {
        User user = getMockUser();
        User user2 = getTestUser();

        Event event = new Event();
        event.setOwner(user2);
        event.setDate(new Date());
        event.setName("SomeEvent");
        event.setUsers(singletonList(user));
        event = eventRepository.save(event);

        Song song = getTestSong(user2, event);

        songsService.voteSong(song, user, true);
        songsService.voteSong(song, user, false);

        //System.out.println(voteRepository.findAll());

        assertThat(voteRepository.findAll()).hasSize(1).containsOnly(new Vote(song, user, true));
    }

    @Test
    @WithMockUser(username = "TestUser", password = "TestPassword")
    public void createShouldReturnCreatedSong() {
        User user = getMockUser();
        Event event = getTestEvent(user);

        Song song = new Song("SomeSong", "SomeInterpret", event, user, "SomePlatform", "SomeLink", 90);

        AddSongDTO dto = new AddSongDTO();
        dto.setTitle("SomeSong");
        dto.setArtist("SomeInterpret");
        dto.setPlatform("SomePlatform");
        dto.setPlaylink("SomeLink");
        dto.setDuration(90);

        Song created = songsService.create(dto, event, user);
        assertThat(created).isEqualToIgnoringGivenFields(song, "id", "suggestedAt");
        assertThat(created.getSuggestedAt()).isNotEqualTo(null);
    }

    @Test
    @WithMockUser(username = "TestUser", password = "TestPassword")
    public void getHistoryShouldReturnListOfPlayedSongs() {
        User user = getMockUser();
        Event event = getTestEvent(user);
        Event event2 = getTestEvent(user);

        SongHistory song1 = new SongHistory();
            song1.setEvent(event);
            song1.setName("Song1");
            song1.setInterpret("Interpret1");
            song1.setDuration(1);
            song1.setPlatform("platform1");
            song1.setPlayLink("Link1");
            song1.setSuggestedAt(new Date());
            song1.setVotes(1);
        SongHistory song2 = new SongHistory();
            song2.setEvent(event2);
            song2.setName("Song2");
            song2.setInterpret("Interpret2");
            song2.setDuration(2);
            song2.setPlatform("platform2");
            song2.setPlayLink("Link2");
            song2.setSuggestedAt(new Date());
            song2.setVotes(2);
        SongHistory song3 = new SongHistory();
            song3.setEvent(event);
            song3.setName("Song3");
            song3.setInterpret("Interpret3");
            song3.setDuration(3);
            song3.setPlatform("platform3");
            song3.setPlayLink("Link3");
            song3.setSuggestedAt(new Date());
            song3.setVotes(3);

        song1 = songHistoryRepository.save(song1);
        song2 = songHistoryRepository.save(song2);
        song3 = songHistoryRepository.save(song3);

        SongHistory finalSong1 = song1;
        SongHistory finalSong2 = song3;

        List<SongHistory> historyForEvent = songsService.getHistoryForEvent(event);
        assertThat(historyForEvent).hasSize(2);
        assertThat(historyForEvent.stream().filter(e -> e.getId().equals(finalSong1.getId())).findFirst().get()).isEqualToIgnoringGivenFields(song1, "event", "suggestedAt", "playedAt");
        assertThat(historyForEvent.stream().filter(e -> e.getId().equals(finalSong2.getId())).findFirst().get()).isEqualToIgnoringGivenFields(song3, "event", "suggestedAt", "playedAt");
    }

    @Test
    public void archiveSong() {
        Event event = new Event();
        event.setName("testevent");
        event.setDate(getDate());
        User user = userRepository.save(new User("TestUser", "TestPassword", "test@domain.tld"));
        event.setOwner(user);
        event = eventRepository.save(event);

        Song song = new Song("SomeSong", "SomeInterpret", event, user, "SomePlatform", "SomeLink", 0);
        song.setId(UUID.randomUUID());
        song = songRepository.save(song);

        this.songsService.archiveSong(song);

        final Event newEvent = this.eventRepository.findById(event.getId()).get();
        assertThat(newEvent.getSongs().size()).isEqualTo(0);
        assertThat(newEvent.getPlayedSongs().size()).isEqualTo(1);
    }
    
    private Event getTestEvent(User owner) {
        Event event = new Event();
        event.setOwner(owner);
        event.setDate(new Date());
        event.setName("SomeEvent");
        return eventRepository.save(event);
    }

    private User getMockUser() {
        User user = new User();
        user.setUsername("TestUser");
        user.setPassword("TestPassword");
        user.setEmail("test@domain.tld");
        return userRepository.save(user);
    }

    private Date getDate() {
        // Only get Date without time so setting hours, minutes, seconds and millis to zero
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    private User getTestUser() {
        User user2 = new User();
        user2.setUsername("TestUser2");
        user2.setPassword("TestPassword2");
        user2.setEmail("otherTest@domain.tld");
        return userRepository.save(user2);
    }

    private Song getTestSong(User user, Event event) {
        Song song = new Song("SomeSong", "SomeInterpret", event, user, "SomePlatform", "SomeLink", 0);
        song.setId(UUID.randomUUID());
        return songRepository.save(song);
    }
}
