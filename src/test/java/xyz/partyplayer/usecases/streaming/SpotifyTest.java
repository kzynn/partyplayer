package xyz.partyplayer.usecases.streaming;


import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;
import xyz.partyplayer.entity.externalApi.SpotifyExternalResponseAccessToken;
import xyz.partyplayer.entity.persistence.SpotifyToken;
import xyz.partyplayer.entity.persistence.User;
import xyz.partyplayer.entity.service.AbstractRequestAccess;
import xyz.partyplayer.entity.service.SpotifyRequestAccess;
import xyz.partyplayer.knowledge.externalApi.SpotifyApi;
import xyz.partyplayer.knowledge.repository.UserRepository;
import xyz.partyplayer.tech.exception.CreateCreditsException;

import java.util.InputMismatchException;
import java.util.Optional;

public class SpotifyTest {

    @Ignore
    @Test(expected = InputMismatchException.class)
    public void createCreditsThrowsErrorIfWrongClassComesIn() throws Exception {
        Spotify spotify = new Spotify(null, null, null, null);
        spotify.createCredits(new WrongCreditsClass());
    }

    @Ignore
    @Test(expected = NullPointerException.class)
    public void createCreditsThrowsErrorIfNullComesIn() throws Exception {
        Spotify spotify = new Spotify(null, null, null, null);
        spotify.createCredits(null);
    }

    @Ignore
    @Test
    public void createCreditsCallGetAccessToken() throws Exception {
        String code = "any code you like";
        SpotifyApi api = Mockito.mock(SpotifyApi.class);
        Mockito.when(api.getAccessTokenAndRefreshToken(Mockito.anyString())).thenReturn(new SpotifyExternalResponseAccessToken("","", "", "", ""));
        UserRepository repo = Mockito.mock(UserRepository.class);

        User user = new User();
        user.setSpotifyToken(new SpotifyToken());
        Mockito.when(repo.findById(Mockito.anyLong())).thenReturn(Optional.of(user));
        Spotify spotify = new Spotify(api, null, null, null);
        spotify.createCredits(new SpotifyRequestAccess(code));


        Mockito.verify(api, Mockito.times(1)).getAccessTokenAndRefreshToken(code);
    }

    @Ignore
    @Test(expected = CreateCreditsException.class)
    public void createCreditsGotErrorFromApi() throws Exception {
        SpotifyApi api = Mockito.mock(SpotifyApi.class);
        Mockito.doThrow(CreateCreditsException.class).when(api).getAccessTokenAndRefreshToken(Mockito.any());

        UserRepository repo = Mockito.mock(UserRepository.class);
        Mockito.when(repo.findById(Mockito.anyLong())).thenReturn(Optional.of(new User()));
        Spotify spotify = new Spotify(api, null, null, null);
        spotify.createCredits(Mockito.mock(SpotifyRequestAccess.class));
    }


    private static class WrongCreditsClass extends AbstractRequestAccess {

    }

}
