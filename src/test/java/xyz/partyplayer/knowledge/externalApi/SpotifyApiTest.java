package xyz.partyplayer.knowledge.externalApi;

import org.junit.Ignore;
import org.junit.Test;
import xyz.partyplayer.entity.externalApi.SpotifyExternalResponseAccessToken;

public class SpotifyApiTest {

    private String clientId = "f4a594d3c768443eb13af4a222bda19b";
    private String secret = "0e1c2001c6954785bca0cd94c51ade1a";
    private String uri = "https://partyplayer.xyz/account/addcredentials/spotify";
    private String xxxx=  "AQDUadNMxM_VsfigDT77QQ_qhkjVHoha6uc7WYEueL9RwT2xZuooQ8aarbLqH73nx-BdTnl27k9Blyu9MlQsiWPVRKRDzAqzkAyLIeK9DQUEEH2v46q2ijANSjJEAYZHN-2nnKWiH1bl3FYjp_IDr2L88kd7o_DWPgHSAKCsi9Byg_i_mR46zhVt_YOiDj6ZOxxAhUHQjqmCMIKRqHMk36EECEQDU9Mhpe2YslslfCCBoT0VO6Bq_f1FIUVP5LrQJUTUQ6xcLS6kRk2FQeitHA";
    private String refreshToken = "AQAksstu4vZvi_hP-UpN7bePdOPOzKkTuQOAX5JyEm-H88wibQHH3Ftq0QZ62jx7b3PpS6e162eIulo4KZJvqNfV5ZxuCiTP2P_RKfLB4O1qw7K_8UmjlLezx9VjIp8QBs8";


    @Test
    @Ignore
    public void GetAccessTokenGetToken() throws Exception {
        SpotifyApi api = new SpotifyApi(clientId, secret, uri);
        SpotifyExternalResponseAccessToken accessTokenAndRefreshToken = api.getAccessTokenAndRefreshToken(xxxx);
        System.out.println(accessTokenAndRefreshToken.getRefresh_token());
    }

    @Test
    @Ignore
    public void GetRefreshToken() throws Exception{
        SpotifyApi api = new SpotifyApi(clientId, secret, "");
        api.refreshAccessToken(refreshToken);
        System.out.println(api.refreshAccessToken(refreshToken).getAccess_token());
    }

    @Test
    @Ignore
    public void SeeDevices() throws Exception{
        SpotifyApi api = new SpotifyApi(clientId, secret, "");
        api.searchDevices(refreshToken);
    }

    @Test
    @Ignore
    public void playSong() throws Exception{
        SpotifyApi api = new SpotifyApi(clientId, secret, "");
        api.playSong(refreshToken,"9d19a768a54f7b189b79ccc38a39a633c9b985f3", "spotify:track:4iV5W9uYEdYUVa79Axb7Rh", "spotify:track:60a0Rd6pjrkxjPbaKzXjfq");
    }

    @Test
    @Ignore
    public void SearchSong() throws Exception {
        SpotifyApi api = new SpotifyApi(clientId, secret, uri);
        api.searchSong("In the end", refreshToken);
    }
    
}
