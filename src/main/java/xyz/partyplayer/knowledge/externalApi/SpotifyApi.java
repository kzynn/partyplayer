package xyz.partyplayer.knowledge.externalApi;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import xyz.partyplayer.entity.externalApi.SpotifyExternalResponseAccessToken;
import xyz.partyplayer.entity.externalApi.SpotifyExternalResponseRefreshToken;
import xyz.partyplayer.entity.persistence.Song;
import xyz.partyplayer.tech.api.model.SearchResult;
import xyz.partyplayer.tech.exception.CreateCreditsException;

import java.io.IOException;
import java.util.*;

public class SpotifyApi {

    private final String redirectUrl;
    private final String authorizationHeader;
    private final CloseableHttpClient client;


    public SpotifyApi(String clientId, String clientSecret, String redirectUrl) {
        this.authorizationHeader = "Basic " + new String(Base64.getEncoder().encode((clientId + ":" + clientSecret).getBytes()));
        this.redirectUrl = redirectUrl;
        this.client = HttpClients.createDefault();
    }


    public SpotifyExternalResponseAccessToken getAccessTokenAndRefreshToken(String code) throws CreateCreditsException {
        HttpClient client = HttpClients.createDefault();

        HttpPost post = new HttpPost("https://accounts.spotify.com/api/token");
        post.setHeader("Content-Type", "application/x-www-form-urlencoded");
        post.setHeader("Authorization", authorizationHeader);

        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicHeader("grant_type", "authorization_code"));
        params.add(new BasicHeader("code", code));
        params.add(new BasicHeader("redirect_uri", redirectUrl));

        try {
            post.setEntity(new UrlEncodedFormEntity(params));
            HttpResponse execute = client.execute(post);

            String res = EntityUtils.toString(execute.getEntity());
            System.out.println(res);
            return new Gson().fromJson(res, SpotifyExternalResponseAccessToken.class);

        } catch (IOException e) {
            throw new CreateCreditsException("spotify api could not perform request");
        }
    }

    public SpotifyExternalResponseRefreshToken refreshAccessToken(String code) throws CreateCreditsException {
        HttpClient client = HttpClients.createDefault();

        HttpPost post = new HttpPost("https://accounts.spotify.com/api/token");
        post.setHeader("Content-Type", "application/x-www-form-urlencoded");
        post.setHeader("Authorization", authorizationHeader);

        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicHeader("grant_type", "refresh_token"));
        params.add(new BasicHeader("refresh_token", code));

        try {
            post.setEntity(new UrlEncodedFormEntity(params));
            HttpResponse execute = client.execute(post);

            String res = EntityUtils.toString(execute.getEntity());
            return new Gson().fromJson(res, SpotifyExternalResponseRefreshToken.class);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;

    }

    public List<SearchResult> searchSong(String search, String token) throws Exception {
        String auth = refreshAccessToken(token).getAccess_token();
        HttpGet get = new HttpGet("https://api.spotify.com/v1/search?q=" + search.replaceAll(" ", "%20") + "&type=track&market=DE&limit=10");
        get.setHeader("Content-Type", "application/x-www-form-urlencoded");
        get.setHeader("Authorization", "Bearer " + auth);
        try {
            HttpResponse execute = client.execute(get);

            String res = EntityUtils.toString(execute.getEntity());
            JsonObject jobj = new Gson().fromJson(res, JsonObject.class);

            List<SearchResult> searchResults = new ArrayList<>();

            jobj.get("tracks").getAsJsonObject().get("items").getAsJsonArray().forEach(e -> {
                JsonObject parse = e.getAsJsonObject();
                SearchResult result = new SearchResult();
                result.setName(parse.get("name").getAsString());
                result.setAuthor(parse.get("album").getAsJsonObject().get("artists").getAsJsonArray().get(0).getAsJsonObject().get("name").getAsString());
                result.setPlatform("Spotify");
                result.setUniqueAccess(parse.get("uri").getAsString());
                searchResults.add(result);
            });

            return searchResults;


        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void searchDevices(String token) throws Exception {
        String auth = refreshAccessToken(token).getAccess_token();
        HttpGet get = new HttpGet("https://api.spotify.com//v1/me/player/devices");
        get.setHeader("Content-Type", "application/x-www-form-urlencoded");
        get.setHeader("Authorization", "Bearer " + auth);
        try {
            HttpResponse execute = client.execute(get);

            String res = EntityUtils.toString(execute.getEntity());
            System.out.println(res);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void playSong(String token, String device, String... trackIds) throws Exception {
        String auth = refreshAccessToken(token).getAccess_token();
        StringBuilder builder = new StringBuilder();
        String seperator = "";
        for (String trackId : trackIds) {
            builder.append(seperator);
            seperator = ",";
            builder.append("\"");
            builder.append(trackId);
            builder.append("\"");
        }

        HttpPut get = new HttpPut("https://api.spotify.com/v1/me/player/play?device_id=" + device);
        get.setHeader("Content-Type", "application/json");
        get.setHeader("Authorization", "Bearer " + auth);
        get.setEntity(new StringEntity("{\"uris\": [" + builder.toString() + "]}"));

        try {
            HttpResponse execute = client.execute(get);

            if (execute.getStatusLine().getStatusCode() != 204) {
                String res = EntityUtils.toString(execute.getEntity());
                System.out.println(res);
            }

            assert execute.getStatusLine().getStatusCode() == 204;
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public Song searchById(String id, String token) throws Exception {
        String auth = refreshAccessToken(token).getAccess_token();
        HttpGet get = new HttpGet("https://api.spotify.com/v1/tracks?ids=" + id.split(":")[2]);
        get.setHeader("Content-Type", "application/x-www-form-urlencoded");
        get.setHeader("Authorization", "Bearer " + auth);
        try {
            HttpResponse execute = client.execute(get);

            String res = EntityUtils.toString(execute.getEntity());
            System.out.println(res);

            String name;
            String interpreter;
            String platfrom = "Spotify";
            String playLink = id;
            int duration;

            JsonObject jobj = new Gson().fromJson(res, JsonObject.class);
            JsonElement track = jobj.getAsJsonArray("tracks").get(0);
            duration = track.getAsJsonObject().get("duration_ms").getAsInt() / 1000;
            name = track.getAsJsonObject().get("name").getAsString();
            interpreter = track.getAsJsonObject().get("artists").getAsJsonArray().get(0).getAsJsonObject().get("name").getAsString();

            Song song= new Song();
            song.setName(name);
            song.setInterpret(interpreter);
            song.setPlatform(platfrom);
            song.setPlayLink(playLink);
            song.setDuration(duration);
            song.setSuggestedAt(Calendar.getInstance().getTime());
            song.setVotes(new HashSet<>());

            return song;


        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
