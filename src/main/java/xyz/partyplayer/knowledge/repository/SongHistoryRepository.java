package xyz.partyplayer.knowledge.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import xyz.partyplayer.entity.persistence.Event;
import xyz.partyplayer.entity.persistence.SongHistory;

import java.util.List;
import java.util.UUID;

@Repository
public interface SongHistoryRepository extends CrudRepository<SongHistory, UUID> {
    List<SongHistory> findAllByEvent(Event event);
}
