package xyz.partyplayer.knowledge.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import xyz.partyplayer.entity.persistence.Vote;
import xyz.partyplayer.entity.persistence.VoteId;

@Repository
public interface VoteRepository extends CrudRepository<Vote, VoteId> {
}
