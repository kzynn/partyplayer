package xyz.partyplayer.knowledge.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import xyz.partyplayer.entity.persistence.Song;

import java.util.UUID;

@Repository
public interface SongRepository extends CrudRepository<Song, UUID> {

    @Query(
            value = "SELECT " +
                    "CASE WHEN count(u.id)=1 THEN 'TRUE' ELSE 'FALSE' END " +
                    "FROM user u JOIN event e " +
                    "ON u.id = e.owner_id OR u.id IN (SELECT ue.user_id FROM users_events ue WHERE ue.event_id = e.id) " +
                    "JOIN song s ON s.event_id = e.id " +
                    "AND s.id = ?1 AND u.id = ?2",
            nativeQuery = true
    )
    boolean isUserMemberOfEvent(String songId, String userID);

}
