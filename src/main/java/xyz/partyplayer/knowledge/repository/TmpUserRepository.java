package xyz.partyplayer.knowledge.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import xyz.partyplayer.entity.persistence.TmpUser;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface TmpUserRepository extends CrudRepository<TmpUser, Long> {

    Optional<TmpUser> findById(UUID id);
}
