package xyz.partyplayer.knowledge.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import xyz.partyplayer.entity.persistence.PasswordReset;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface PasswordResetRepository extends CrudRepository<PasswordReset, Long> {
    Optional<PasswordReset> findById(UUID id);

    Optional<PasswordReset> findByToken(String token);
}
