package xyz.partyplayer.knowledge.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import xyz.partyplayer.entity.persistence.Event;
import xyz.partyplayer.entity.persistence.User;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface EventRepository extends CrudRepository<Event, UUID> {

    @Query("SELECT e FROM Event e WHERE ?1 member of e.users OR ?1 = e.owner")
    List<Event> findAllByUser(User user);

    @Query("SELECT e FROM Event e WHERE e.id = ?1 AND (?2 member of e.users OR ?2 = e.owner)")
    Optional<Event> findByIdAndUser(UUID eventId, User user);

    void deleteEventById(UUID id);
}
