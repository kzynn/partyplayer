package xyz.partyplayer.knowledge.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import xyz.partyplayer.entity.persistence.SpotifyToken;

import java.util.UUID;

@Repository
public interface SpotifyTokenRepository extends CrudRepository<SpotifyToken, UUID> {
    boolean existsByUser_id(UUID user_id);
    long deleteByUser_id(UUID user_id);
}
