package xyz.partyplayer.tech.exception;

public class ExpiredJwtException extends Exception {

    public ExpiredJwtException(String message) {
        super(message);
    }
}
