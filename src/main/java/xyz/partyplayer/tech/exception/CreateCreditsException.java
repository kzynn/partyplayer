package xyz.partyplayer.tech.exception;

public class CreateCreditsException extends Exception {

    public CreateCreditsException(String message) {
        super(message);
    }
}
