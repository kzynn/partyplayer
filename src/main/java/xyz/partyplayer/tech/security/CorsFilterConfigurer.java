package xyz.partyplayer.tech.security;

import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;

public class CorsFilterConfigurer extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {

    public CorsFilterConfigurer() {

    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        CorsFilter filter = new CorsFilter();
        http.addFilterBefore(filter, JwtTokenFilter.class);
    }

}
