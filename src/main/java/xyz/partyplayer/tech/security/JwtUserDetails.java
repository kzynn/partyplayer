package xyz.partyplayer.tech.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import xyz.partyplayer.entity.persistence.Role;
import xyz.partyplayer.entity.persistence.TmpUser;
import xyz.partyplayer.entity.persistence.User;
import xyz.partyplayer.knowledge.repository.TmpUserRepository;
import xyz.partyplayer.knowledge.repository.UserRepository;
import xyz.partyplayer.tech.api.model.AuthUser;
import xyz.partyplayer.tech.exception.UserNotFoundException;

import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;

@Service
public class JwtUserDetails implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TmpUserRepository tmpUserRepository;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final User user = userRepository.findByUsername(username);

        if (user == null) {
            throw new UsernameNotFoundException("User '" + username + "' not found");
        }

        return org.springframework.security.core.userdetails.User//
                .withUsername(username)//
                .password(user.getPassword())//
                .accountExpired(false)//
                .accountLocked(false)//
                .credentialsExpired(false)//
                .disabled(false)//
                .build();
    }

    UserDetails loadUserById(String id) throws UserNotFoundException {
        final Optional<User> userOpt = userRepository.findById(UUID.fromString(id));
        if (!userOpt.isPresent()) {
            final Optional<TmpUser> tmpUser = tmpUserRepository.findById(UUID.fromString(id));

            if (!tmpUser.isPresent()) {
                throw new UserNotFoundException("User with id '" + id + "' not found");
            }

            TmpUser user = tmpUser.get();

            ArrayList<Role> lst = new ArrayList<Role>();

            return new AuthUser(String.valueOf(user.getId()), "", true,
                    true, true, true, lst,
                    String.valueOf(tmpUser.get().getEvent().getId()));
        } else {
            User user = userOpt.get();

            return org.springframework.security.core.userdetails.User//
                    .withUsername(user.getUsername())//
                    .password(user.getPassword())//
                    .authorities(user.getRoles())//
                    .accountExpired(false)//
                    .accountLocked(false)//
                    .credentialsExpired(false)//
                    .disabled(false)//
                    .build();
        }
    }
}
