package xyz.partyplayer.tech.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import xyz.partyplayer.entity.persistence.PasswordReset;
import xyz.partyplayer.entity.persistence.User;
import xyz.partyplayer.knowledge.repository.PasswordResetRepository;
import xyz.partyplayer.knowledge.repository.UserRepository;
import xyz.partyplayer.tech.api.model.*;
import xyz.partyplayer.tech.api.service.CredentialService;
import xyz.partyplayer.tech.api.service.UserService;
import xyz.partyplayer.tech.security.JwtTokenProvider;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static sun.security.pkcs11.wrapper.Functions.toHexString;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-11-08T13:30:14.941Z")

@Controller
public class AccountApiController implements AccountApi {

    @Autowired
    UserService userService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordResetRepository passwordResetRepository;

    @Autowired
    CredentialService credentialService;

    @Autowired
    EmailSender emailSender;

    private static final Logger log = LoggerFactory.getLogger(AccountApiController.class);
    private final ObjectMapper objectMapper;
    private final JwtTokenProvider jwtTokenProvider = new JwtTokenProvider();
    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public AccountApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<UserDataResponse> accountData() {
        UserDataResponse userData = new UserDataResponse();

        User user = userService.getAuthenticatedUser();
        userData.setEmail(user.getEmail());
        userData.setCredentials(credentialService.getCredentials(user));

        return new ResponseEntity<UserDataResponse>(userData, HttpStatus.OK);
    }

    public ResponseEntity<JwtResponse> accountLogin(
            @ApiParam(value = "User Credentials", required = true) @Valid @RequestBody UserCredentials body
    ) {
        try {
            User user = authenticate(body.getUsername(), body.getPassword());

            jwtTokenProvider.init();
            final String token = jwtTokenProvider.createToken(user);

            JwtResponse response = new JwtResponse();
            response.setToken(token);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new ResponseEntity<JwtResponse>(HttpStatus.FORBIDDEN);
    }

    public ResponseEntity<UserCreationResponse> accountRegister(
            @ApiParam(value = "User Data", required = true) @Valid @RequestBody CreateUserDTO body
    ) {
        UserCreationResponse response = new UserCreationResponse();
        List<String> errors = new ArrayList<>();

        HttpStatus status = HttpStatus.BAD_REQUEST;

        if (userRepository.existsByUsername(body.getUsername())) {
            errors.add("username");
        }

        if (userRepository.existsByEmail(body.getEmail())) {
            errors.add("email");
        }

        if (errors.isEmpty()) {
            String link = null;
            try {
                link = "https://partyplayer.xyz/account/activate/" + body.getEmail() + "/" + toHexString(getSHA(body.getEmail()));
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }

            try {
                SimpleMailMessage message = new SimpleMailMessage();
                String to = body.getEmail();
                String subject = "PartyPlayer - Account verification";
                String text = "Hi " + body.getUsername() + ". Um deinen Account zu benutzen musst du noch deine Email-Adresse bestätigen. Folge hierzu bitte diesem Link: " + link;

                emailSender.sendEmail(to, subject, text);
            } catch (Exception e) {
                System.out.println(link);
            }

            User user = userService.create(body.getUsername(), body.getPassword(), body.getEmail());
            response.setUser(UserResponse.from(user));
            status = HttpStatus.CREATED;
            // MISSING: SEND ACTIVATION EMAIL
        }

        response.setError(errors);


        return new ResponseEntity<UserCreationResponse>(response, status);
    }

    @Override
    public ResponseEntity<Void> deleteAccount() {
        this.userService.delete();
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    public ResponseEntity<Void> accountActivate(@ApiParam(value = "email of account to activate", required = true) @PathVariable("email") String email,
                                                @ApiParam(value = "token to validate email", required = true) @PathVariable("token") String token) {

        HttpStatus status;
        try {
            if (!toHexString(getSHA(email)).equals(token)) {
                status = HttpStatus.UNAUTHORIZED;
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        User user = userRepository.findByEmail(email);
        user.setEmailVerifiedAt(new Date());
        userRepository.save(user);

        status = HttpStatus.OK;

        return new ResponseEntity<Void>(status);
    }

    public ResponseEntity<UserResponse> updateUsername(@ApiParam(value = "Username" ,required=true )
                                                       @Valid @RequestBody UpdateUsernameDTO updateUsernameDTO) {
        try {
            if(updateUsernameDTO.getUsername().isEmpty())
                return new ResponseEntity<UserResponse>(HttpStatus.BAD_REQUEST);

            User user = userService.getAuthenticatedUser();
            user.setUsername(updateUsernameDTO.getUsername());
            userRepository.save(user);

            return new ResponseEntity<UserResponse>(UserResponse.from(user), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<UserResponse>(HttpStatus.UNAUTHORIZED);
        }
    }

    private User authenticate(String username, String password) throws Exception {
        User user = userRepository.findByUsername(username);
        if (user == null || !user.getPassword().equals(password)) {
            throw new Exception("Invalid Password");
        }
        return user;
    }

    public static byte[] getSHA(String input) throws NoSuchAlgorithmException {
        // Static getInstance method is called with hashing SHA
        MessageDigest md = MessageDigest.getInstance("SHA-256");

        // digest() method called
        // to calculate message digest of an input
        // and return array of byte
        return md.digest(input.getBytes(StandardCharsets.UTF_8));
    }

    public ResponseEntity<Void> passwordreset(@ApiParam(value = "email of account to set new pasword for", required = true) @PathVariable("email") String email,
                                              @ApiParam(value = "token to validate email", required = true) @PathVariable("token") String token,
                                              @ApiParam(value = "password", required = true) @Valid @RequestBody PasswordResetDTO body) {

        Optional<PasswordReset> passwordReset = passwordResetRepository.findByToken(token);

        if(!passwordReset.isPresent()) {
            return new ResponseEntity<Void>(HttpStatus.UNAUTHORIZED);
        }

        if(!passwordReset.get().getUser().getEmail().equals(email)) {
            return new ResponseEntity<Void>(HttpStatus.UNAUTHORIZED);
        }

        User user = passwordReset.get().getUser();
        user.setPassword(body.getPassword());

        userRepository.save(user);

        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    public ResponseEntity<Void> passwordresetInit(@ApiParam(value = "email of account to init reset for", required = true) @PathVariable("email") String email) {
        String token = "";
        try {
            token = toHexString(AccountApiController.getSHA(email + new Date()));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        User user = userRepository.findByEmail(email);

        if (user != null) {
            PasswordReset passwordreset = new PasswordReset();
            passwordreset.setToken(token);
            passwordreset.setUser(user);

            passwordResetRepository.save(passwordreset);

            String link;
            link = "https://partyplayer.xyz/account/passwordreset/" + email + "/" + token;
             try {
                SimpleMailMessage message = new SimpleMailMessage();
                String to = user.getEmail();
                String subject = "PartyPlayer - Passwordreset";
                String text = "Du möchtest dein Passwort zurücksetzen?. Folge hierzu bitte diesem Link: " + link;

                emailSender.sendEmail(to, subject, text);
            } catch (Exception e) {
                System.out.println(link);
            }

            System.out.println(link);

        }

        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}
