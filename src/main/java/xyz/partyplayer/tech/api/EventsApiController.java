package xyz.partyplayer.tech.api;

import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import xyz.partyplayer.entity.persistence.Event;
import xyz.partyplayer.entity.persistence.Song;
import xyz.partyplayer.entity.persistence.SongHistory;
import xyz.partyplayer.entity.persistence.User;
import xyz.partyplayer.tech.api.model.*;
import xyz.partyplayer.tech.api.service.EventsService;
import xyz.partyplayer.tech.api.service.SongsService;
import xyz.partyplayer.tech.api.service.UserService;

import javax.validation.Valid;
import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2020-05-19T12:28:05.476+02:00")

@Controller
public class EventsApiController implements EventsApi {

    @Autowired
    private EventsService eventsService;

    @Autowired
    private SongsService songsService;

    @Autowired
    private UserService userService;

    public ResponseEntity<List<EventResponse>> getAllEventsForUser() {
        User user = userService.getAuthenticatedUser();
        List<Event> events = eventsService.getAllForUser(user);

        List<EventResponse> reponses = events.stream().map(EventResponse::from).collect(Collectors.toList());

        return new ResponseEntity<List<EventResponse>>(reponses, HttpStatus.OK);
    }

    public ResponseEntity<EventResponse> getEventById(
            @ApiParam(value = "Unique ID of Event", required = true) @PathVariable("id") UUID id
    ) {
        Optional<Event> event = eventsService.getById(id);

        if (!event.isPresent() || !this.userService.isOwnerMemberOrTmpUser(event.get())) {
            return new ResponseEntity<EventResponse>(HttpStatus.NOT_FOUND);
        }

        event.get().setRelation(this.eventsService.getRelation(event.get()));

        return new ResponseEntity<EventResponse>(EventResponse.from(event.get()), HttpStatus.OK);
    }

    public ResponseEntity<EventResponse> deleteEventById(
            @ApiParam(value = "Unique ID of Event", required = true) @PathVariable("id") UUID id
    ) {
        Optional<Event> event = eventsService.getById(id);

        if (event.isPresent()) {
            if (userService.isOwner(userService.getAuthenticatedUser(), event.get())) {
                this.eventsService.deleteById(id);
                return new ResponseEntity<EventResponse>(HttpStatus.OK);
            }

            return new ResponseEntity<EventResponse>(HttpStatus.UNAUTHORIZED);
        }

        return new ResponseEntity<EventResponse>(HttpStatus.NOT_FOUND);
    }

    public ResponseEntity<EventResponse> eventCreate(
            @ApiParam(value = "Data needed to create new event", required = true) @Valid @RequestBody CreateEventDTO createEventDto
    ) {
        ResponseDateFormat iso_df = new ResponseDateFormat();
        Date date = null;
        try {
            date = iso_df.parse(createEventDto.getDate().replaceAll("Z$", "+0000"));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Event createdEvent = eventsService.create(createEventDto.getName(), createEventDto.getDescription(),
                date, userService.getAuthenticatedUser());

        return new ResponseEntity<EventResponse>(EventResponse.from(createdEvent), HttpStatus.CREATED);
    }

    public ResponseEntity<List<UserResponse>> eventGetMembers(
            @ApiParam(value = "Unique ID of Event", required = true) @PathVariable("id") UUID id) {

        try {
            User authUser = userService.getAuthenticatedUser();
            Optional<Event> event = eventsService.getByIdAndUser(id, authUser);
            if (!event.isPresent()) {
                return new ResponseEntity<List<UserResponse>>(HttpStatus.NOT_FOUND);
            }

            List<User> user = eventsService.getMembersOfEvent(event.get());
            List<UserResponse> users = new ArrayList<>();
            users.addAll(user.stream().map(u -> UserResponse.from(u)).collect(Collectors.toList()));

            return new ResponseEntity<List<UserResponse>>(users, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<List<UserResponse>>(HttpStatus.UNAUTHORIZED);
        }
    }

    public ResponseEntity<List<SongResponse>> eventGetSongs(
            @ApiParam(value = "Unique ID of Event", required = true) @PathVariable("id") UUID id
    ) {
        Optional<Event> event = eventsService.getById(id);

        if (!event.isPresent() || !this.userService.isOwnerMemberOrTmpUser(event.get())) {
            return new ResponseEntity<List<SongResponse>>(HttpStatus.NOT_FOUND);
        }

        List<SongResponse> responses = event.get().getSongs().parallelStream().map(SongResponse::from).collect(Collectors.toList());
        return new ResponseEntity<List<SongResponse>>(responses, HttpStatus.OK);
    }

    public ResponseEntity<SongResponse> eventAddSong(
            @ApiParam(value = "Unique ID of Event", required = true) @PathVariable("id") UUID id,
            @ApiParam(value = "Information about the song to add", required = true) @Valid @RequestBody AddSongDTO addSongDTO
    ) {
        Optional<Event> event = eventsService.getByIdAndUser(id, userService.getAuthenticatedUser());

        if (!event.isPresent()) {
            return new ResponseEntity<SongResponse>(HttpStatus.NOT_FOUND);
        }

        Song song = songsService.create(addSongDTO, event.get(), userService.getAuthenticatedUser());
        return ResponseEntity.ok(SongResponse.from(song));
    }

    public ResponseEntity<UserResponse> eventInviteUser(
            @ApiParam(value = "Unique ID of Event", required = true) @PathVariable("id") UUID id,
            @ApiParam(value = "Username to invite", required = true) @Valid @RequestBody InviteUserDTO inviteUserDTO
    ) {
        Optional<Event> event = eventsService.getById(id);

        if (!event.isPresent()) {
            return new ResponseEntity<UserResponse>(HttpStatus.NOT_FOUND);
        }

        User user = userService.getAuthenticatedUser();

        if (!userService.isOwner(user, event.get())) {
            return new ResponseEntity<UserResponse>(HttpStatus.FORBIDDEN);
        }

        eventsService.addUserToEvent(event.get(), inviteUserDTO.getUsername(), user);
        return new ResponseEntity<UserResponse>(HttpStatus.OK);
    }

    public ResponseEntity<JwtResponse> eventGuestJWT(
            @ApiParam(value = "Unique ID of Event", required = true) @PathVariable("id") UUID id,
            @ApiParam(value = "", required = true) @Valid @RequestBody CreateJWTInvitation createJWTInvitation
    ) {
        Optional<Event> event = eventsService.getById(id);

        if (!event.isPresent()) {
            return new ResponseEntity<JwtResponse>(HttpStatus.NOT_FOUND);
        }

        try {
            String token = eventsService.temporaryJoin(event.get(), createJWTInvitation);
            JwtResponse res = new JwtResponse();
            res.setToken(token);
            return new ResponseEntity<JwtResponse>(res, HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new ResponseEntity<JwtResponse>(HttpStatus.UNAUTHORIZED);
    }

    public ResponseEntity<InvitationLinkResponse> eventJoinLink(
            @ApiParam(value = "Unique ID of Event", required = true) @PathVariable("id") UUID id
    ) {
        try {
            String link = eventsService.createJoinLink(id, userService.getAuthenticatedUser());
            InvitationLinkResponse res = new InvitationLinkResponse();
            res.setLink(link);

            return new ResponseEntity<InvitationLinkResponse>(res, HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new ResponseEntity<InvitationLinkResponse>(HttpStatus.FORBIDDEN);
    }

    public ResponseEntity<UserResponse> eventRemoveMember(
            @ApiParam(value = "Unique ID of Event", required = true) @PathVariable("id") UUID id,
            @ApiParam(value = "ID of user to remove", required = true) @PathVariable("userId") UUID userId) {

        try {
            Optional<Event> event = eventsService.getByIdAndUser(id, userService.getAuthenticatedUser());
            Optional<User> user = userService.getById(userId);

            if (!user.isPresent() || !event.isPresent()) {
                return new ResponseEntity<UserResponse>(HttpStatus.NOT_FOUND);
            } else if (!userService.getAuthenticatedUser().equals(event.get().getOwner())) {
                return new ResponseEntity<UserResponse>(HttpStatus.FORBIDDEN);
            }

            eventsService.removeMemberFromEvent(event.get(), user.get());
            return new ResponseEntity<UserResponse>(UserResponse.from(user.get()), HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<UserResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<Void> acceptInvitation(
            @ApiParam(value = "Unique ID of Event", required = true) @PathVariable("id") UUID id,
            @ApiParam(value = "Invitation Link of Event", required = true) @PathVariable("link") String link
    ) {
        Optional<Event> event = eventsService.getById(id);

        if (!event.isPresent()) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }

        try {
            eventsService.acceptInvitation(event.get(), link, userService.getAuthenticatedUser());
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
    }

    public ResponseEntity<EventResponse> updateEventById(
            @ApiParam(value = "Unique ID of Event", required = true) @PathVariable("id") UUID id,
            @ApiParam(value = "Data needed to update event", required = true) @Valid @RequestBody CreateEventDTO createEventDTO
    ) {
        try {
            User user = userService.getAuthenticatedUser();
            Optional<Event> event = eventsService.getByIdAndUser(id, user);

            if (event.isPresent()) {
                Event e = event.get();
                if (userService.isOwner(user, e)) {
                    e.setName(createEventDTO.getName());
                    e.setDescription(createEventDTO.getDescription());

                    Date date = eventsService.parseDateFromString(createEventDTO.getDate());
                    e.setDate(date);

                    eventsService.saveEvent(e);
                    return new ResponseEntity<EventResponse>(EventResponse.from(e), HttpStatus.OK);
                }
                return new ResponseEntity<EventResponse>(HttpStatus.UNAUTHORIZED);
            }

            return new ResponseEntity<EventResponse>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<EventResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<List<SongHistoryResponse>> getPlayedSongs(
            @ApiParam(value = "Unique ID of Event", required = true) @PathVariable("id") UUID id
    ) {
        Optional<Event> event = this.eventsService.getByIdAndUser(id, this.userService.getAuthenticatedUser());

        if (!event.isPresent()) {
            return new ResponseEntity<List<SongHistoryResponse>>(HttpStatus.NOT_FOUND);
        }

        List<SongHistory> historyForEvent = this.songsService.getHistoryForEvent(event.get());
        List<SongHistoryResponse> response = historyForEvent.parallelStream().map(SongHistoryResponse::from).collect(Collectors.toList());
        return new ResponseEntity<List<SongHistoryResponse>>(response, HttpStatus.OK);
    }

    public ResponseEntity<Void> moveSongToHistory(
            @ApiParam(value = "Unique ID of Event", required = true) @PathVariable("eventId") UUID eventId,
            @ApiParam(value = "Unique ID of Song to push to history", required = true) @PathVariable("songId") UUID songId
    ) {
        Optional<Event> event = this.eventsService.getByIdAndUser(eventId, this.userService.getAuthenticatedUser());
        Song song = this.songsService.getById(songId);

        if (!event.isPresent() || song == null) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }

        this.songsService.archiveSong(song);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

}
