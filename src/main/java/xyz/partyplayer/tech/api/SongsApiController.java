package xyz.partyplayer.tech.api;


import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import xyz.partyplayer.entity.persistence.Song;
import xyz.partyplayer.entity.persistence.User;
import xyz.partyplayer.tech.api.model.SearchResult;
import xyz.partyplayer.tech.api.service.SongsService;
import xyz.partyplayer.tech.api.service.UserService;
import xyz.partyplayer.usecases.streaming.Spotify;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2020-05-19T12:28:05.476+02:00")

@Controller
public class SongsApiController implements SongsApi {

    private static final Logger log = LoggerFactory.getLogger(SongsApiController.class);

    private final Spotify spotify;
    private final HttpServletRequest request;

    @Autowired
    private SongsService songsService;

    @Autowired
    private UserService userService;

    @Autowired
    public SongsApiController(HttpServletRequest request, Spotify spotify) {
        this.request = request;
        this.spotify = spotify;
    }

    public ResponseEntity<List<SearchResult>> searchSong(
        @ApiParam(value = "Title of song to search for") @Valid @RequestParam(value = "title", required = false) String title
    ) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<List<SearchResult>>(spotify.searchSongByTitle(title), HttpStatus.OK);
            } catch (Exception e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<List<SearchResult>>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<List<SearchResult>>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> voteSongDown(
        @ApiParam(value = "Unique ID of song to downvote", required = true) @PathVariable("id") UUID id
    ) {
        User user = userService.getAuthenticatedUser();
        Song song = songsService.getById(id);

        if (song == null) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }

        songsService.voteSong(song, user, false);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    public ResponseEntity<Void> voteSongUp(
        @ApiParam(value = "Unique ID of the song to upvote", required = true) @PathVariable("id") UUID id
    ) {
        User user = userService.getAuthenticatedUser();
        Song song = songsService.getById(id);

        if (song == null) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }

        songsService.voteSong(song, user, true);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}
