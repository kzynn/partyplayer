package xyz.partyplayer.tech.api;

import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import xyz.partyplayer.entity.persistence.User;
import xyz.partyplayer.entity.service.SpotifyRequestAccess;
import xyz.partyplayer.tech.api.model.Code;
import xyz.partyplayer.tech.api.model.CreateSpotifyPlayTokenDTO;
import xyz.partyplayer.tech.api.model.SpotifyWebToken;
import xyz.partyplayer.tech.api.service.CredentialService;
import xyz.partyplayer.tech.api.service.SpotifyService;
import xyz.partyplayer.tech.api.service.UserService;
import xyz.partyplayer.tech.exception.CreateCreditsException;
import xyz.partyplayer.usecases.IPlatformCredentials;

import javax.validation.Valid;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-05-14T03:09:23.493Z")

@Controller
public class SpotifyApiController implements SpotifyApi {

    private final CredentialService credentialService;
    private final UserService userService;

    @Autowired
    private SpotifyService spotifyService;

    @Autowired
    private IPlatformCredentials spotifyCredentails;

    @Autowired
    public SpotifyApiController(CredentialService credentialService, UserService userService) {
        this.credentialService = credentialService;
        this.userService = userService;
    }

    public ResponseEntity<Void> deleteAccessToken() {
        User user = userService.getAuthenticatedUser();

        if (credentialService.deleteSpotifyToken(user)) {
            return new ResponseEntity<>(HttpStatus.OK);
        }

        return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
    }

    public ResponseEntity<Void> createAccessToken(@ApiParam(value = "Spotify Token", required = true) @Valid @RequestBody Code body) {
        try {
            this.spotifyCredentails.createCredits(new SpotifyRequestAccess(body.getCode()));
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (CreateCreditsException e) {
            e.printStackTrace();
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<SpotifyWebToken> createSpotifyTokenToPlay(@ApiParam(value = "Data to create playlink for spotify", required = true) @Valid @RequestBody CreateSpotifyPlayTokenDTO createSpotifyPlayTokenDTO) {
        this.spotifyService.playSong(createSpotifyPlayTokenDTO.getPlayLink(), createSpotifyPlayTokenDTO.getDevice());
        return new ResponseEntity<SpotifyWebToken>(HttpStatus.OK);
    }

    public ResponseEntity<SpotifyWebToken> getSpotifyTokenToPlay() {
        SpotifyWebToken res = this.spotifyService.getPlayToken(userService.getAuthenticatedUser().getId());
        return new ResponseEntity<SpotifyWebToken>(res, HttpStatus.OK);
    }

}
