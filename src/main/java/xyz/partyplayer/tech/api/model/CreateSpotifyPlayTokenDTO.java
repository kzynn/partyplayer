package xyz.partyplayer.tech.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import java.util.Objects;


/**
 * CreateSpotifyPlayTokenDTO
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-05-19T16:22:29.186Z")

public class CreateSpotifyPlayTokenDTO   {
  @JsonProperty("playLink")
  private String playLink = null;

  @JsonProperty("device")
  private String device = null;

  public CreateSpotifyPlayTokenDTO playLink(String playLink) {
    this.playLink = playLink;
    return this;
  }

  /**
   * Get playLink
   * @return playLink
   **/
  @ApiModelProperty(value = "")


  public String getPlayLink() {
    return playLink;
  }

  public void setPlayLink(String playLink) {
    this.playLink = playLink;
  }

  public CreateSpotifyPlayTokenDTO device(String device) {
    this.device = device;
    return this;
  }

  /**
   * Get device
   * @return device
   **/
  @ApiModelProperty(value = "")


  public String getDevice() {
    return device;
  }

  public void setDevice(String device) {
    this.device = device;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CreateSpotifyPlayTokenDTO createSpotifyPlayTokenDTO = (CreateSpotifyPlayTokenDTO) o;
    return Objects.equals(this.playLink, createSpotifyPlayTokenDTO.playLink) &&
            Objects.equals(this.device, createSpotifyPlayTokenDTO.device);
  }

  @Override
  public int hashCode() {
    return Objects.hash(playLink, device);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CreateSpotifyPlayTokenDTO {\n");

    sb.append("    playLink: ").append(toIndentedString(playLink)).append("\n");
    sb.append("    device: ").append(toIndentedString(device)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}