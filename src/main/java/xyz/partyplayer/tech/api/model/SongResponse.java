package xyz.partyplayer.tech.api.model;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Objects;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import xyz.partyplayer.entity.persistence.Song;

import javax.validation.Valid;

/**
 * SongResponse
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-05-31T10:38:16.140Z")

public class SongResponse   {
    @JsonProperty("id")
    private UUID id = null;

    @JsonProperty("title")
    private String title = null;

    @JsonProperty("duration")
    private Integer duration = null;

    @JsonProperty("platform")
    private String platform = null;

    @JsonProperty("playlink")
    private String playlink = null;

    @JsonProperty("artist")
    private String artist = null;

    @JsonProperty("votes")
    private Integer votes = null;

    @JsonProperty("suggestedAt")
    private OffsetDateTime suggestedAt = null;

    public SongResponse id(UUID id) {
        this.id = id;
        return this;
    }

    /**
     * Get id
     * @return id
     **/
    @ApiModelProperty(value = "")

    @Valid

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public SongResponse title(String title) {
        this.title = title;
        return this;
    }

    /**
     * Get title
     * @return title
     **/
    @ApiModelProperty(value = "")


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public SongResponse duration(Integer duration) {
        this.duration = duration;
        return this;
    }

    /**
     * Get duration
     * @return duration
     **/
    @ApiModelProperty(value = "")


    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public SongResponse platform(String platform) {
        this.platform = platform;
        return this;
    }

    /**
     * Get platform
     * @return platform
     **/
    @ApiModelProperty(value = "")


    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public SongResponse playlink(String playlink) {
        this.playlink = playlink;
        return this;
    }

    /**
     * Get playlink
     * @return playlink
     **/
    @ApiModelProperty(value = "")


    public String getPlaylink() {
        return playlink;
    }

    public void setPlaylink(String playlink) {
        this.playlink = playlink;
    }

    public SongResponse artist(String artist) {
        this.artist = artist;
        return this;
    }

    /**
     * Get artist
     * @return artist
     **/
    @ApiModelProperty(value = "")


    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public SongResponse votes(Integer votes) {
        this.votes = votes;
        return this;
    }

    /**
     * Get votes
     * @return votes
     **/
    @ApiModelProperty(value = "")


    public Integer getVotes() {
        return votes;
    }

    public void setVotes(Integer votes) {
        this.votes = votes;
    }

    public SongResponse suggestedAt(OffsetDateTime suggestedAt) {
        this.suggestedAt = suggestedAt;
        return this;
    }

    /**
     * Timestamp when this song has been suggested
     * @return suggestedAt
     **/
    @ApiModelProperty(value = "Timestamp when this song has been suggested")

    @Valid

    public OffsetDateTime getSuggestedAt() {
        return suggestedAt;
    }

    public void setSuggestedAt(OffsetDateTime suggestedAt) {
        this.suggestedAt = suggestedAt;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SongResponse songResponse = (SongResponse) o;
        return Objects.equals(this.id, songResponse.id) &&
                Objects.equals(this.title, songResponse.title) &&
                Objects.equals(this.duration, songResponse.duration) &&
                Objects.equals(this.platform, songResponse.platform) &&
                Objects.equals(this.playlink, songResponse.playlink) &&
                Objects.equals(this.artist, songResponse.artist) &&
                Objects.equals(this.votes, songResponse.votes) &&
                Objects.equals(this.suggestedAt, songResponse.suggestedAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, duration, platform, playlink, artist, votes, suggestedAt);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class SongResponse {\n");

        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    title: ").append(toIndentedString(title)).append("\n");
        sb.append("    duration: ").append(toIndentedString(duration)).append("\n");
        sb.append("    platform: ").append(toIndentedString(platform)).append("\n");
        sb.append("    playlink: ").append(toIndentedString(playlink)).append("\n");
        sb.append("    artist: ").append(toIndentedString(artist)).append("\n");
        sb.append("    votes: ").append(toIndentedString(votes)).append("\n");
        sb.append("    suggestedAt: ").append(toIndentedString(suggestedAt)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }

    public static SongResponse from(Song song) {
        return new SongResponse()
                .id(song.getId())
                .title(song.getName())
                .artist(song.getInterpret())
                .duration(song.getDuration())
                .platform(song.getPlatform())
                .playlink(song.getPlayLink())
                .votes(song.getVoteCount())
                .suggestedAt(song.getSuggestedAt().toInstant()
                        .atOffset(ZoneOffset.UTC));
    }
}

