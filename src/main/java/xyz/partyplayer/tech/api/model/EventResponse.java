package xyz.partyplayer.tech.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import xyz.partyplayer.entity.persistence.Event;

import javax.validation.Valid;
import java.util.Objects;
import java.util.UUID;

/**
 * EventResponse
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-05-31T10:38:16.140Z")

public class EventResponse {
    @JsonProperty("id")
    private UUID id = null;

    @JsonProperty("name")
    private String name = null;

    @JsonProperty("description")
    private String description = null;

    @JsonProperty("invitationLink")
    private String invitationLink = null;

    @JsonProperty("relation")
    private String relation = null;

    @JsonProperty("date")
    private String date = null;

    public EventResponse id(UUID id) {
        this.id = id;
        return this;
    }

    public static EventResponse from(Event event) {
        ResponseDateFormat iso_df = new ResponseDateFormat();
        return new EventResponse()
                .id(event.getId())
                .name(event.getName())
                .invitationLink(event.getInvitationLink())
                .description(event.getDescription())
                .relation(event.getRelation())
                .date(iso_df.format(event.getDate()).replaceAll("\\+0000", "Z"));
    }

    /**
     * Get id
     *
     * @return id
     **/
    @ApiModelProperty(value = "")

    @Valid

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public EventResponse name(String name) {
        this.name = name;
        return this;
    }

    /**
     * Get name
     *
     * @return name
     **/
    @ApiModelProperty(value = "")


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EventResponse description(String description) {
        this.description = description;
        return this;
    }

    /**
     * Get description
     *
     * @return description
     **/
    @ApiModelProperty(value = "")


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public EventResponse invitationLink(String invitationLink) {
        this.invitationLink = invitationLink;
        return this;
    }

    /**
     * Get invitationLink
     *
     * @return invitationLink
     **/
    @ApiModelProperty(value = "")


    public String getInvitationLink() {
        return invitationLink;
    }

    public void setInvitationLink(String invitationLink) {
        this.invitationLink = invitationLink;
    }

    public EventResponse relation(String relation) {
        this.relation = relation;
        return this;
    }

    /**
     * Get relation
     *
     * @return relation
     **/
    @ApiModelProperty(value = "")


    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public EventResponse date(String date) {
        this.date = date;
        return this;
    }

    /**
     * Get date
     *
     * @return date
     **/
    @ApiModelProperty(value = "")

    @Valid

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        EventResponse eventResponse = (EventResponse) o;
        return Objects.equals(this.id, eventResponse.id) &&
                Objects.equals(this.name, eventResponse.name) &&
                Objects.equals(this.description, eventResponse.description) &&
                Objects.equals(this.invitationLink, eventResponse.invitationLink) &&
                Objects.equals(this.relation, eventResponse.relation) &&
                Objects.equals(this.date, eventResponse.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, invitationLink, relation, date);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class EventResponse {\n");

        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    name: ").append(toIndentedString(name)).append("\n");
        sb.append("    description: ").append(toIndentedString(description)).append("\n");
        sb.append("    invitationLink: ").append(toIndentedString(invitationLink)).append("\n");
        sb.append("    relation: ").append(toIndentedString(relation)).append("\n");
        sb.append("    date: ").append(toIndentedString(date)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}