package xyz.partyplayer.tech.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


/**
 * UserCreationResponse
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-05-19T16:22:29.186Z")

public class UserCreationResponse   {
  @JsonProperty("user")
  private UserResponse user = null;

  @JsonProperty("error")
  @Valid
  private List<String> error = null;

  public UserCreationResponse user(UserResponse user) {
    this.user = user;
    return this;
  }

  /**
   * Get user
   * @return user
   **/
  @ApiModelProperty(value = "")

  @Valid

  public UserResponse getUser() {
    return user;
  }

  public void setUser(UserResponse user) {
    this.user = user;
  }

  public UserCreationResponse error(List<String> error) {
    this.error = error;
    return this;
  }

  public UserCreationResponse addErrorItem(String errorItem) {
    if (this.error == null) {
      this.error = new ArrayList<String>();
    }
    this.error.add(errorItem);
    return this;
  }

  /**
   * Get error
   * @return error
   **/
  @ApiModelProperty(value = "")


  public List<String> getError() {
    return error;
  }

  public void setError(List<String> error) {
    this.error = error;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UserCreationResponse userCreationResponse = (UserCreationResponse) o;
    return Objects.equals(this.user, userCreationResponse.user) &&
            Objects.equals(this.error, userCreationResponse.error);
  }

  @Override
  public int hashCode() {
    return Objects.hash(user, error);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UserCreationResponse {\n");

    sb.append("    user: ").append(toIndentedString(user)).append("\n");
    sb.append("    error: ").append(toIndentedString(error)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

