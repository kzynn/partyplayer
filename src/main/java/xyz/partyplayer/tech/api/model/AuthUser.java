package xyz.partyplayer.tech.api.model;

import org.springframework.security.core.userdetails.User;

import java.util.Collection;

public class AuthUser extends User {

    private static final long serialVersionUID = -3531439484732724601L;

    private String eventId;

    public AuthUser(String username, String password, boolean enabled,
                    boolean accountNonExpired, boolean credentialsNonExpired,
                    boolean accountNonLocked,
                    Collection authorities,
                    String eventId) {

        super(username, password, enabled, accountNonExpired,
                credentialsNonExpired, accountNonLocked, authorities);

        this.eventId = eventId;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public void setEventId(String id) {
        this.eventId = id;
    }

    public String getEventId() {
        return this.eventId;
    }

}



