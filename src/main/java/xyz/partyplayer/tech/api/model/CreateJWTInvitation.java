package xyz.partyplayer.tech.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import java.util.Objects;


/**
 * CreateJWTInvitation
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-05-19T16:22:29.186Z")

public class CreateJWTInvitation   {
  @JsonProperty("invitationLink")
  private String invitationLink = null;

  public CreateJWTInvitation invitationLink(String invitationLink) {
    this.invitationLink = invitationLink;
    return this;
  }

  /**
   * Get invitationLink
   * @return invitationLink
   **/
  @ApiModelProperty(value = "")


  public String getInvitationLink() {
    return invitationLink;
  }

  public void setInvitationLink(String invitationLink) {
    this.invitationLink = invitationLink;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CreateJWTInvitation createJWTInvitation = (CreateJWTInvitation) o;
    return Objects.equals(this.invitationLink, createJWTInvitation.invitationLink);
  }

  @Override
  public int hashCode() {
    return Objects.hash(invitationLink);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CreateJWTInvitation {\n");

    sb.append("    invitationLink: ").append(toIndentedString(invitationLink)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}