package xyz.partyplayer.tech.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@AllArgsConstructor
@Data
public class SseEvent {
    private UUID event;
    private Object data;
}
