package xyz.partyplayer.tech.api.model;

import java.text.SimpleDateFormat;
import java.util.TimeZone;

public class ResponseDateFormat extends SimpleDateFormat {
    public ResponseDateFormat() {
        super("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        this.setTimeZone(TimeZone.getTimeZone("UTC"));
    }
}
