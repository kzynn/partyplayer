package xyz.partyplayer.tech.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import xyz.partyplayer.entity.persistence.SongHistory;

import javax.validation.Valid;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Objects;
import java.util.UUID;

/**
 * SongHistoryResponse
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-05-31T10:38:16.140Z")

public class SongHistoryResponse {
    @JsonProperty("id")
    private UUID id = null;

    @JsonProperty("title")
    private String title = null;

    @JsonProperty("duration")
    private Integer duration = null;

    @JsonProperty("platform")
    private String platform = null;

    @JsonProperty("playlink")
    private String playlink = null;

    @JsonProperty("artist")
    private String artist = null;

    @JsonProperty("votes")
    private Integer votes = null;

    @JsonProperty("suggestedAt")
    private OffsetDateTime suggestedAt = null;

    @JsonProperty("playedAt")
    private OffsetDateTime playedAt = null;

    public SongHistoryResponse id(UUID id) {
        this.id = id;
        return this;
    }

    /**
     * Get id
     *
     * @return id
     **/
    @ApiModelProperty(value = "")

    @Valid

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public SongHistoryResponse title(String title) {
        this.title = title;
        return this;
    }

    /**
     * Get title
     *
     * @return title
     **/
    @ApiModelProperty(value = "")


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public SongHistoryResponse duration(Integer duration) {
        this.duration = duration;
        return this;
    }

    /**
     * Get duration
     *
     * @return duration
     **/
    @ApiModelProperty(value = "")


    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public SongHistoryResponse platform(String platform) {
        this.platform = platform;
        return this;
    }

    /**
     * Get platform
     *
     * @return platform
     **/
    @ApiModelProperty(value = "")


    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public SongHistoryResponse playlink(String playlink) {
        this.playlink = playlink;
        return this;
    }

    /**
     * Get playlink
     *
     * @return playlink
     **/
    @ApiModelProperty(value = "")


    public String getPlaylink() {
        return playlink;
    }

    public void setPlaylink(String playlink) {
        this.playlink = playlink;
    }

    public SongHistoryResponse artist(String artist) {
        this.artist = artist;
        return this;
    }

    /**
     * Get artist
     *
     * @return artist
     **/
    @ApiModelProperty(value = "")


    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public SongHistoryResponse votes(Integer votes) {
        this.votes = votes;
        return this;
    }

    /**
     * Get votes
     *
     * @return votes
     **/
    @ApiModelProperty(value = "")


    public Integer getVotes() {
        return votes;
    }

    public void setVotes(Integer votes) {
        this.votes = votes;
    }

    public SongHistoryResponse suggestedAt(OffsetDateTime suggestedAt) {
        this.suggestedAt = suggestedAt;
        return this;
    }

    /**
     * Timestamp when this song has been suggested
     *
     * @return suggestedAt
     **/
    @ApiModelProperty(value = "Timestamp when this song has been suggested")

    @Valid

    public OffsetDateTime getSuggestedAt() {
        return suggestedAt;
    }

    public void setSuggestedAt(OffsetDateTime suggestedAt) {
        this.suggestedAt = suggestedAt;
    }

    public SongHistoryResponse playedAt(OffsetDateTime playedAt) {
        this.playedAt = playedAt;
        return this;
    }

    /**
     * Timestamp when this song has been played
     *
     * @return playedAt
     **/
    @ApiModelProperty(value = "Timestamp when this song has been played")

    @Valid

    public OffsetDateTime getPlayedAt() {
        return playedAt;
    }

    public void setPlayedAt(OffsetDateTime playedAt) {
        this.playedAt = playedAt;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SongHistoryResponse songHistoryResponse = (SongHistoryResponse) o;
        return Objects.equals(this.id, songHistoryResponse.id) &&
                Objects.equals(this.title, songHistoryResponse.title) &&
                Objects.equals(this.duration, songHistoryResponse.duration) &&
                Objects.equals(this.platform, songHistoryResponse.platform) &&
                Objects.equals(this.playlink, songHistoryResponse.playlink) &&
                Objects.equals(this.artist, songHistoryResponse.artist) &&
                Objects.equals(this.votes, songHistoryResponse.votes) &&
                Objects.equals(this.suggestedAt, songHistoryResponse.suggestedAt) &&
                Objects.equals(this.playedAt, songHistoryResponse.playedAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, duration, platform, playlink, artist, votes, suggestedAt, playedAt);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class SongHistoryResponse {\n");

        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    title: ").append(toIndentedString(title)).append("\n");
        sb.append("    duration: ").append(toIndentedString(duration)).append("\n");
        sb.append("    platform: ").append(toIndentedString(platform)).append("\n");
        sb.append("    playlink: ").append(toIndentedString(playlink)).append("\n");
        sb.append("    artist: ").append(toIndentedString(artist)).append("\n");
        sb.append("    votes: ").append(toIndentedString(votes)).append("\n");
        sb.append("    suggestedAt: ").append(toIndentedString(suggestedAt)).append("\n");
        sb.append("    playedAt: ").append(toIndentedString(playedAt)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }

    public static SongHistoryResponse from(SongHistory songHistory) {
        return new SongHistoryResponse()
                .id(songHistory.getId())
                .title(songHistory.getName())
                .artist(songHistory.getInterpret())
                .duration(songHistory.getDuration())
                .platform(songHistory.getPlatform())
                .playlink(songHistory.getPlayLink())
                .votes(songHistory.getVotes())
                .suggestedAt(songHistory.getSuggestedAt().toInstant()
                        .atOffset(ZoneOffset.UTC))
                .playedAt(songHistory.getPlayedAt().toInstant()
                        .atOffset(ZoneOffset.UTC));
    }
}

