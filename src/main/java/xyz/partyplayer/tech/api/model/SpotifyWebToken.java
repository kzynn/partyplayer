package xyz.partyplayer.tech.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import java.util.Objects;

/**
 * SpotifyWebToken
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-05-19T16:22:29.186Z")

public class SpotifyWebToken   {
  @JsonProperty("spotifyWebToken")
  private String spotifyWebToken = null;

  public SpotifyWebToken spotifyWebToken(String spotifyWebToken) {
    this.spotifyWebToken = spotifyWebToken;
    return this;
  }

  /**
   * Get spotifyWebToken
   * @return spotifyWebToken
  **/
  @ApiModelProperty(value = "")


  public String getSpotifyWebToken() {
    return spotifyWebToken;
  }

  public void setSpotifyWebToken(String spotifyWebToken) {
    this.spotifyWebToken = spotifyWebToken;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SpotifyWebToken spotifyWebToken = (SpotifyWebToken) o;
    return Objects.equals(this.spotifyWebToken, spotifyWebToken.spotifyWebToken);
  }

  @Override
  public int hashCode() {
    return Objects.hash(spotifyWebToken);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SpotifyWebToken {\n");
    
    sb.append("    spotifyWebToken: ").append(toIndentedString(spotifyWebToken)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

