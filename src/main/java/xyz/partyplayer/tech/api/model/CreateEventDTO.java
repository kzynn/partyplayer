package xyz.partyplayer.tech.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import java.util.Objects;

/**
 * CreateEventDTO
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-05-19T16:22:29.186Z")

public class CreateEventDTO   {
  @JsonProperty("name")
  private String name = null;

  @JsonProperty("description")
  private String description = null;

  @JsonProperty("date")
  private String date = null;

  public CreateEventDTO name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Name of the created event
   * @return name
   **/
  @ApiModelProperty(value = "Name of the created event")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public CreateEventDTO description(String description) {
    this.description = description;
    return this;
  }

  /**
   * Description of the created event
   * @return description
   **/
  @ApiModelProperty(value = "Description of the created event")


  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public CreateEventDTO date(String date) {
    this.date = date;
    return this;
  }

  /**
   * Date on which the event will take place
   * @return date
   **/
  @ApiModelProperty(value = "Date on which the event will take place")


  public String getDate() {
    return date;
  }

  public void setDate(String date) {
    this.date = date;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CreateEventDTO createEventDTO = (CreateEventDTO) o;
    return Objects.equals(this.name, createEventDTO.name) &&
            Objects.equals(this.description, createEventDTO.description) &&
            Objects.equals(this.date, createEventDTO.date);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, description, date);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CreateEventDTO {\n");

    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    date: ").append(toIndentedString(date)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

