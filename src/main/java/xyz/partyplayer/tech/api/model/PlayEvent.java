package xyz.partyplayer.tech.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class PlayEvent {
    private String id;
}
