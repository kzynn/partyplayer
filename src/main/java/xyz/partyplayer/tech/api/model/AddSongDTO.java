package xyz.partyplayer.tech.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import java.util.Objects;

/**
 * AddSongDTO
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-05-19T16:22:29.186Z")

public class AddSongDTO   {
    @JsonProperty("title")
    private String title = null;

    @JsonProperty("duration")
    private Integer duration = null;

    @JsonProperty("platform")
    private String platform = null;

    @JsonProperty("playlink")
    private String playlink = null;

    @JsonProperty("artist")
    private String artist = null;

    public AddSongDTO title(String title) {
        this.title = title;
        return this;
    }

    /**
     * Get title
     * @return title
     **/
    @ApiModelProperty(value = "")


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public AddSongDTO duration(Integer duration) {
        this.duration = duration;
        return this;
    }

    /**
     * Get duration
     * @return duration
     **/
    @ApiModelProperty(value = "")


    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public AddSongDTO platform(String platform) {
        this.platform = platform;
        return this;
    }

    /**
     * Get platform
     * @return platform
     **/
    @ApiModelProperty(value = "")


    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public AddSongDTO playlink(String playlink) {
        this.playlink = playlink;
        return this;
    }

    /**
     * Get playlink
     * @return playlink
     **/
    @ApiModelProperty(value = "")


    public String getPlaylink() {
        return playlink;
    }

    public void setPlaylink(String playlink) {
        this.playlink = playlink;
    }

    public AddSongDTO artist(String artist) {
        this.artist = artist;
        return this;
    }

    /**
     * Get artist
     * @return artist
     **/
    @ApiModelProperty(value = "")


    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }


    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AddSongDTO addSongDTO = (AddSongDTO) o;
        return Objects.equals(this.title, addSongDTO.title) &&
                Objects.equals(this.duration, addSongDTO.duration) &&
                Objects.equals(this.platform, addSongDTO.platform) &&
                Objects.equals(this.playlink, addSongDTO.playlink) &&
                Objects.equals(this.artist, addSongDTO.artist);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, duration, platform, playlink, artist);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class AddSongDTO {\n");

        sb.append("    title: ").append(toIndentedString(title)).append("\n");
        sb.append("    duration: ").append(toIndentedString(duration)).append("\n");
        sb.append("    platform: ").append(toIndentedString(platform)).append("\n");
        sb.append("    playlink: ").append(toIndentedString(playlink)).append("\n");
        sb.append("    artist: ").append(toIndentedString(artist)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}