package xyz.partyplayer.tech.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class VoteEvent {
    private String id;
    private int votes;
}
