package xyz.partyplayer.tech.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import reactor.core.publisher.Flux;
import xyz.partyplayer.tech.api.service.ServerSentEventService;

import java.util.UUID;

@Api(value = "sse", description = "The api for SSEs")
@Controller
public class ServerSentEventController {

    @Autowired
    private ServerSentEventService sseService;

    @GetMapping(path = "/events/{id}/song-flux", produces = "text/event-stream")
    @ApiOperation(value = "Subscribe to sse of event", notes = "", authorizations = {
            @Authorization(value = "Bearer")
    }, tags = {"Event"})
    public Flux<ServerSentEvent<String>> songFlux(@PathVariable("id") @ApiParam(value = "Unique ID of Event to subscribe to", required = true) UUID id) {
        return sseService.getSongEventsForEvent(id);
    }
}
