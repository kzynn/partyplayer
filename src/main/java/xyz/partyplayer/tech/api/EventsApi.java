/**
 * NOTE: This class is auto generated by the swagger code generator program (2.4.14).
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */
package xyz.partyplayer.tech.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import xyz.partyplayer.tech.api.model.AddSongDTO;
import xyz.partyplayer.tech.api.model.CreateEventDTO;
import xyz.partyplayer.tech.api.model.CreateJWTInvitation;
import xyz.partyplayer.tech.api.model.EventResponse;
import xyz.partyplayer.tech.api.model.InvitationLinkResponse;
import xyz.partyplayer.tech.api.model.InviteUserDTO;
import xyz.partyplayer.tech.api.model.JwtResponse;
import xyz.partyplayer.tech.api.model.SongHistoryResponse;
import xyz.partyplayer.tech.api.model.SongResponse;
import xyz.partyplayer.tech.api.model.UserResponse;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2020-05-31T10:38:16.140Z")

@Api(value = "events", description = "the events API")
public interface EventsApi {

    @ApiOperation(value = "Create Event join link", nickname = "acceptInvitation", notes = "", authorizations = {
            @Authorization(value = "Bearer")
    }, tags = {"Event",})
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Invitation Accepted"),
            @ApiResponse(code = 401, message = "Invalid or Missing Authorization Header"),
            @ApiResponse(code = 403, message = "Invalid permissions"),
            @ApiResponse(code = 404, message = "Event or link does not exist")})
    @RequestMapping(value = "/events/{id}/link/{link}/accept",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ResponseEntity<Void> acceptInvitation(@ApiParam(value = "Unique ID of Event", required = true) @PathVariable("id") UUID id, @ApiParam(value = "Invitation Link of Event", required = true) @PathVariable("link") String link);


    @ApiOperation(value = "Delete event by id", nickname = "deleteEventById", notes = "", response = EventResponse.class, authorizations = {
            @Authorization(value = "Bearer")
    }, tags = {"Event",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successfully deleted event operation", response = EventResponse.class),
            @ApiResponse(code = 401, message = "Invalid or Missing Authorization Header"),
            @ApiResponse(code = 403, message = "Invalid permissions"),
            @ApiResponse(code = 404, message = "Event does not exist")})
    @RequestMapping(value = "/events/{id}",
            produces = {"application/json"},
            method = RequestMethod.DELETE)
    ResponseEntity<EventResponse> deleteEventById(@ApiParam(value = "Unique ID of Event", required = true) @PathVariable("id") UUID id);


    @ApiOperation(value = "Add song to Event", nickname = "eventAddSong", notes = "", response = SongResponse.class, authorizations = {
            @Authorization(value = "Bearer")
    }, tags = {"Event",})
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Song created successfully", response = SongResponse.class),
            @ApiResponse(code = 401, message = "Invalid or Missing Authorization Header"),
            @ApiResponse(code = 403, message = "Invalid permissions"),
            @ApiResponse(code = 404, message = "Event does not exist"),
            @ApiResponse(code = 405, message = "Invalid Song Object")})
    @RequestMapping(value = "/events/{id}/songs",
            consumes = {"application/json"},
            method = RequestMethod.POST)
    ResponseEntity<SongResponse> eventAddSong(@ApiParam(value = "Unique ID of Event", required = true) @PathVariable("id") UUID id, @ApiParam(value = "Information about the song to add", required = true) @Valid @RequestBody AddSongDTO addSongDTO);


    @ApiOperation(value = "Create new event", nickname = "eventCreate", notes = "Create an event providing necessary information", response = EventResponse.class, authorizations = {
            @Authorization(value = "Bearer")
    }, tags = {"Event",})
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Event created successfully", response = EventResponse.class),
            @ApiResponse(code = 401, message = "Invalid or Missing Authorization Header"),
            @ApiResponse(code = 403, message = "Invalid permissions"),
            @ApiResponse(code = 405, message = "Invalid input")})
    @RequestMapping(value = "/events",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.POST)
    ResponseEntity<EventResponse> eventCreate(@ApiParam(value = "Data needed to create new event", required = true) @Valid @RequestBody CreateEventDTO createEventDTO);

    @ApiOperation(value = "Get list of event members", nickname = "eventGetMembers", notes = "", response = UserResponse.class, responseContainer = "List", authorizations = {
            @Authorization(value = "Bearer")
    }, tags = {"Event",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Event members", response = UserResponse.class, responseContainer = "List"),
            @ApiResponse(code = 401, message = "Invalid or Missing Authorization Header"),
            @ApiResponse(code = 403, message = "Invalid permissions"),
            @ApiResponse(code = 404, message = "Event does not exist"),
            @ApiResponse(code = 405, message = "Invalid input")})
    @RequestMapping(value = "/events/{id}/users",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ResponseEntity<List<UserResponse>> eventGetMembers(@ApiParam(value = "Unique ID of Event", required = true) @PathVariable("id") UUID id);


    @ApiOperation(value = "Get all songs of this Event", nickname = "eventGetSongs", notes = "", response = SongResponse.class, responseContainer = "List", authorizations = {
            @Authorization(value = "Bearer")
    }, tags = {"Event",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = SongResponse.class, responseContainer = "List"),
            @ApiResponse(code = 400, message = "Invalid Input"),
            @ApiResponse(code = 401, message = "Invalid or Missing Authorization Header"),
            @ApiResponse(code = 403, message = "Invalid permissions"),
            @ApiResponse(code = 404, message = "Event does not exist")})
    @RequestMapping(value = "/events/{id}/songs",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ResponseEntity<List<SongResponse>> eventGetSongs(@ApiParam(value = "Unique ID of Event", required = true) @PathVariable("id") UUID id);


    @ApiOperation(value = "Create JWT for guest join", nickname = "eventGuestJWT", notes = "Creates a JWT to allow the user to join", response = JwtResponse.class, tags = {"Event",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "JWT", response = JwtResponse.class),
            @ApiResponse(code = 401, message = "Invalid or Missing Authorization Header"),
            @ApiResponse(code = 403, message = "Invalid permissions"),
            @ApiResponse(code = 404, message = "Event does not exist")})
    @RequestMapping(value = "/events/{id}/guest",
            consumes = {"application/json"},
            method = RequestMethod.POST)
    ResponseEntity<JwtResponse> eventGuestJWT(@ApiParam(value = "Unique ID of Event", required = true) @PathVariable("id") UUID id, @ApiParam(value = "", required = true) @Valid @RequestBody CreateJWTInvitation createJWTInvitation);


    @ApiOperation(value = "Invite User to event", nickname = "eventInviteUser", notes = "", response = UserResponse.class, authorizations = {
            @Authorization(value = "Bearer")
    }, tags = {"Event",})
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Invited user", response = UserResponse.class),
            @ApiResponse(code = 401, message = "Invalid or Missing Authorization Header"),
            @ApiResponse(code = 403, message = "Invalid permissions"),
            @ApiResponse(code = 404, message = "Event or user does not exist"),
            @ApiResponse(code = 405, message = "Invalid input")})
    @RequestMapping(value = "/events/{id}/users",
            consumes = {"application/json"},
            method = RequestMethod.POST)
    ResponseEntity<UserResponse> eventInviteUser(@ApiParam(value = "Unique ID of Event", required = true) @PathVariable("id") UUID id, @ApiParam(value = "Username to invite", required = true) @Valid @RequestBody InviteUserDTO inviteUserDTO);


    @ApiOperation(value = "Create Event join link", nickname = "eventJoinLink", notes = "", response = InvitationLinkResponse.class, authorizations = {
            @Authorization(value = "Bearer")
    }, tags = {"Event",})
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Link created", response = InvitationLinkResponse.class),
            @ApiResponse(code = 401, message = "Invalid or Missing Authorization Header"),
            @ApiResponse(code = 403, message = "Invalid permissions"),
            @ApiResponse(code = 404, message = "Event does not exist")})
    @RequestMapping(value = "/events/{id}/link",
            consumes = {"application/json"},
            method = RequestMethod.POST)
    ResponseEntity<InvitationLinkResponse> eventJoinLink(@ApiParam(value = "Unique ID of Event", required = true) @PathVariable("id") UUID id);


    @ApiOperation(value = "Remove member from event", nickname = "eventRemoveMember", notes = "", response = UserResponse.class, authorizations = {
            @Authorization(value = "Bearer")
    }, tags = {"Event",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Removed member", response = UserResponse.class),
            @ApiResponse(code = 401, message = "Invalid or Missing Authorization Header"),
            @ApiResponse(code = 403, message = "Invalid permissions"),
            @ApiResponse(code = 404, message = "Event or user does not exist"),
            @ApiResponse(code = 405, message = "Invalid input")})
    @RequestMapping(value = "/events/{id}/users/{userId}",
            produces = {"application/json"},
            method = RequestMethod.DELETE)
    ResponseEntity<UserResponse> eventRemoveMember(@ApiParam(value = "Unique ID of Event", required = true) @PathVariable("id") UUID id, @ApiParam(value = "ID of user to remove", required = true) @PathVariable("userId") UUID userId);


    @ApiOperation(value = "Get all events for authenticated user.", nickname = "getAllEventsForUser", notes = "Returns array of all events.", response = EventResponse.class, responseContainer = "List", authorizations = {
            @Authorization(value = "Bearer")
    }, tags = {"Event",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Getting all lists for authenticated user.", response = EventResponse.class, responseContainer = "List"),
            @ApiResponse(code = 401, message = "Invalid or Missing Authorization Header"),
            @ApiResponse(code = 403, message = "Invalid permissions")})
    @RequestMapping(value = "/events",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ResponseEntity<List<EventResponse>> getAllEventsForUser();


    @ApiOperation(value = "Get event by id", nickname = "getEventById", notes = "", response = EventResponse.class, authorizations = {
            @Authorization(value = "Bearer")
    }, tags = {"Event",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = EventResponse.class),
            @ApiResponse(code = 401, message = "Invalid or Missing Authorization Header"),
            @ApiResponse(code = 404, message = "Event does not exist")})
    @RequestMapping(value = "/events/{id}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ResponseEntity<EventResponse> getEventById(@ApiParam(value = "Unique ID of Event", required = true) @PathVariable("id") UUID id);


    @ApiOperation(value = "get list of played songs", nickname = "getPlayedSongs", notes = "get all songs that have been played in this event", response = SongHistoryResponse.class, responseContainer = "List", authorizations = {
            @Authorization(value = "Bearer")
    }, tags = {"Event",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "List of played songs", response = SongHistoryResponse.class, responseContainer = "List"),
            @ApiResponse(code = 401, message = "Invalid or Missing Authorization Header"),
            @ApiResponse(code = 403, message = "Invalid permissions"),
            @ApiResponse(code = 404, message = "Event or song does not exist")})
    @RequestMapping(value = "/events/{id}/history",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ResponseEntity<List<SongHistoryResponse>> getPlayedSongs(@ApiParam(value = "Unique ID of Event", required = true) @PathVariable("id") UUID id);


    @ApiOperation(value = "move song from suggested songs to history", nickname = "moveSongToHistory", notes = "move song from suggested songs to history", authorizations = {
            @Authorization(value = "Bearer")
    }, tags = {"Event",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 401, message = "Invalid or Missing Authorization Header"),
            @ApiResponse(code = 403, message = "Invalid permissions"),
            @ApiResponse(code = 404, message = "Event or song does not exist")})
    @RequestMapping(value = "/events/{eventId}/songs/{songId}/history",
            method = RequestMethod.POST)
    ResponseEntity<Void> moveSongToHistory(@ApiParam(value = "Unique ID of Event", required = true) @PathVariable("eventId") UUID eventId, @ApiParam(value = "Unique ID of Song to push to history", required = true) @PathVariable("songId") UUID songId);


    @ApiOperation(value = "Update event by id", nickname = "updateEventById", notes = "", response = EventResponse.class, authorizations = {
            @Authorization(value = "Bearer")
    }, tags = {"Event",})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "successful operation", response = EventResponse.class),
            @ApiResponse(code = 401, message = "Invalid or Missing Authorization Header"),
            @ApiResponse(code = 404, message = "Event does not exist")})
    @RequestMapping(value = "/events/{id}",
            produces = {"application/json"},
            consumes = {"application/json"},
            method = RequestMethod.POST)
    ResponseEntity<EventResponse> updateEventById(@ApiParam(value = "Unique ID of Event", required = true) @PathVariable("id") UUID id, @ApiParam(value = "Data needed to update event", required = true) @Valid @RequestBody CreateEventDTO createEventDTO);
}
