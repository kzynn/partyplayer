package xyz.partyplayer.tech.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.partyplayer.entity.persistence.*;
import xyz.partyplayer.knowledge.repository.SongHistoryRepository;
import xyz.partyplayer.knowledge.repository.SongRepository;
import xyz.partyplayer.knowledge.repository.VoteRepository;
import xyz.partyplayer.tech.api.model.AddSongDTO;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class SongsService {

    @Autowired
    private ServerSentEventService sseService;

    @Autowired
    private SongRepository songRepository;

    @Autowired
    private SongHistoryRepository songHistoryRepository;

    @Autowired
    private VoteRepository voteRepository;

    public Song getById(UUID id) {
        return songRepository.findById(id).orElse(null);
    }

    public void voteSong(Song song, User user, boolean isUpVote) {
        if (songRepository.isUserMemberOfEvent(song.getId().toString(), user.getId().toString())) {
            voteRepository.save(new Vote(song, user, isUpVote));

            // trigger SSE event
            this.sseService.createVoteEvent(song);
        }
    }

    public Song create(AddSongDTO addSongDTO, Event event, User user) {
        Song song = new Song(addSongDTO.getTitle(), addSongDTO.getArtist(), event, user, addSongDTO.getPlatform(), addSongDTO.getPlaylink(), addSongDTO.getDuration());

        Song createdSong = songRepository.save(song);

        // trigger SSE event
        this.sseService.createAddSongEvent(createdSong, event.getId());

        return createdSong;
    }

    public List<SongHistory> getHistoryForEvent(Event event) {
        return this.songHistoryRepository.findAllByEvent(event);
    }

    public void archiveSong(final Song song) {
        this.songRepository.delete(song);

        SongHistory songHistory = new SongHistory();
        songHistory.setEvent(song.getEvent());
        songHistory.setPlayedAt(new Date());
        songHistory.setSuggestedAt(song.getSuggestedAt());
        songHistory.setVotes(song.getVoteCount());
        songHistory.setDuration(song.getDuration());
        songHistory.setName(song.getName());
        songHistory.setInterpret(song.getInterpret());
        songHistory.setPlayLink(song.getPlayLink());
        songHistory.setPlatform(song.getPlatform());
        songHistory.setId(song.getId());

        this.songHistoryRepository.save(songHistory);

        // trigger SSE event
        this.sseService.createPlaySongEvent(song);
    }
}
