package xyz.partyplayer.tech.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import xyz.partyplayer.entity.persistence.Event;
import xyz.partyplayer.entity.persistence.Song;
import xyz.partyplayer.entity.persistence.Event;
import xyz.partyplayer.entity.persistence.User;
import xyz.partyplayer.entity.persistence.Vote;
import xyz.partyplayer.knowledge.repository.EventRepository;
import xyz.partyplayer.knowledge.repository.SongRepository;
import xyz.partyplayer.knowledge.repository.UserRepository;
import xyz.partyplayer.knowledge.repository.VoteRepository;
import xyz.partyplayer.tech.api.model.AuthUser;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private SongRepository songRepository;

    @Autowired
    private VoteRepository voteRepository;

    public User getAuthenticatedUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return userRepository.findByUsername(authentication.getName());
    }

    public AuthUser getAuthenticatedUserAuthUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        try {
            return (AuthUser) authentication.getPrincipal();
        } catch (Exception e) {
            throw e;
        }

    }

    public User create(String username, String password, String email) {

        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        user.setEmail(email);

        return userRepository.save(user);
    }

    public Optional<User> getById(UUID id) {
        return userRepository.findById(id);
    }

    public boolean isOwnerMemberOrTmpUser(Event event) {
        User user = getAuthenticatedUser();

        if (user != null) {
            return isOwnerOrMember(user, event);
        }

        AuthUser authUser = getAuthenticatedUserAuthUser();

        if (authUser != null) {
            return authUser.getEventId().equals(String.valueOf(event.getId()));
        }

        return false;
    }

    public boolean isOwnerOrMember(User user, Event event) {
        return isMember(user, event) || isOwner(user, event);
    }

    public boolean isMember(User user, Event event) {
        return event.getUsers().contains(user);
    }

    public boolean isOwner(User user, Event event) {
        return event.getOwner().equals(user);
    }

    public void delete() {
        User user = this.getAuthenticatedUser();

        User owner = new User("Deleted User " + user.getId().toString(), "", user.getId().toString());

        final List<Event> allByUser = this.eventRepository.findAllByUser(user);

        owner = this.userRepository.save(owner);

        if (allByUser.size() > 0) {
            owner = this.userRepository.save(owner);

            for (Event e : allByUser) {
                e.setOwner(owner);
                e = this.eventRepository.save(e);
            }
        }

        for (Event e : this.eventRepository.findAll()) {
            for(User u : e.getUsers()){
                if (u.getId().toString().equals(user.getId().toString())) {
                    owner = this.userRepository.save(owner);
                    e.getUsers().remove(u);
                    e.getUsers().add(owner);
                    e = this.eventRepository.save(e);
                    break;
                }
            }

        }

        for (Song e : this.songRepository.findAll()) {
            if(e.getSuggestedBy().getId().toString().equals(user.getId().toString())){
                owner = this.userRepository.save(owner);
                e.setSuggestedBy(owner);
                this.songRepository.save(e);
            }
        }


        for (Vote e : this.voteRepository.findAll()) {
            if(e.getUser().getId().toString().equals(user.getId().toString())){
                owner = this.userRepository.save(owner);
                this.voteRepository.delete(e);
                this.voteRepository.save(new Vote(e.getSong(), owner, e.isUpVote()));
            }
        }


        this.userRepository.delete(user);

    }
}
