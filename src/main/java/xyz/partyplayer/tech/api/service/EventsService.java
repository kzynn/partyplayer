package xyz.partyplayer.tech.api.service;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.partyplayer.entity.persistence.*;
import xyz.partyplayer.knowledge.repository.*;
import xyz.partyplayer.tech.api.model.AuthUser;
import xyz.partyplayer.tech.api.model.CreateJWTInvitation;
import xyz.partyplayer.tech.api.model.ResponseDateFormat;
import xyz.partyplayer.tech.security.JwtTokenProvider;

import java.security.SecureRandom;
import java.text.ParseException;
import java.util.*;

@Service
public class EventsService {

    private final EventRepository eventRepository;
    private final TmpUserRepository tmpUserRepository;
    private final UserRepository userRepository;

    public final UserService userService;

    private final JwtTokenProvider jwtTokenProvider = new JwtTokenProvider();

    @Autowired
    public EventsService(EventRepository eventRepository, TmpUserRepository tmpUserRepository, UserService userService, UserRepository userRepository) {
        this.eventRepository = eventRepository;
        this.tmpUserRepository = tmpUserRepository;
        this.userService = userService;
        this.userRepository = userRepository;
    }

    public Event create(String name, String description, Date date, User user) {
        Event event = new Event();
        event.setName(name);
        event.setDescription(description);
        event.setDate(date);
        event.setOwner(user);

        return eventRepository.save(event);
    }

    public void saveEvent(Event event) {
        eventRepository.save(event);
    }

    public void deleteById(UUID id) {
        eventRepository.deleteById(id);
    }

    public Optional<Event> getById(UUID id) {
        return eventRepository.findById(id);
    }

    public List<Event> getAllForUser(User user) {
        return eventRepository.findAllByUser(user);
    }

    /**
     * Returns event if exists and given user is either owner or member of the event
     */
    public Optional<Event> getByIdAndUser(UUID id, User user) {
        return eventRepository.findByIdAndUser(id, user);
    }

    public void addUserToEvent(Event event, String username, User owner) {
        User user = userRepository.findByUsername(username);

        Hibernate.initialize(event.getUsers());

        List<User> userIds = event.getUsers();

        if (user != null && !userIds.contains(user)) {
            userIds.add(user);
            event.setUsers(userIds);
            eventRepository.save(event);
        }
    }

    public boolean invitationLinkIsValid(Event event, String invitationLink) {
        return event.getInvitationLink().equals(invitationLink);
    }

    public String temporaryJoin(Event event, CreateJWTInvitation createJWTInvitation) throws Exception {
        if (this.invitationLinkIsValid(event, createJWTInvitation.getInvitationLink())) {
            TmpUser tmp = new TmpUser();
            tmp.setEvent(event);
            tmp = tmpUserRepository.save(tmp);

            jwtTokenProvider.init();

            return jwtTokenProvider.createTokenForTemporaryJoin(String.valueOf(tmp.getId()), event.getId());
        }

        throw new Exception();
    }

    public String createJoinLink(UUID id, User authenticatedUser) throws Exception {
        Optional<Event> event = eventRepository.findById(id);

        System.out.println(id);
        System.out.println(event);

        if (!event.isPresent()) {
            throw new Exception("Event not found");
        }

        if (event.get().getOwner().getId() != authenticatedUser.getId()) {
            throw new Exception("Unauthorized");
        }

        String link = this._createRandomString(10);

        event.get().setInvitationLink(link);

        eventRepository.save(event.get());

        return link;
    }

    private String _createRandomString(int length) {
        final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        SecureRandom rnd = new SecureRandom();

        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        }

        return sb.toString();
    }

    public void acceptInvitation(Event event, String link, User user) throws Exception {
        if (!event.getInvitationLink().equals(link)) {
            throw new Exception("Invalid link");
        }

        List<User> userIds = event.getUsers();

        if (!userIds.contains(user)) {
            userIds.add(user);
            event.setUsers(userIds);
            eventRepository.save(event);
        }
    }

    public Date parseDateFromString(String dateString) throws ParseException {
        ResponseDateFormat iso_df = new ResponseDateFormat();
        return iso_df.parse(dateString.replaceAll("Z$", "+0000"));
    }

    public List<User> getMembersOfEvent(Event event) {
        return userRepository.findByEventsContains(event);
    }

    public void removeMemberFromEvent(Event event, User user) {
        Hibernate.initialize(event.getUsers());
        System.out.println(event);

        List<User> users = event.getUsers();

        if (users.contains(user)) {
            users.remove(user);
            event.setUsers(users);
            eventRepository.save(event);
        }
    }

    public String getRelation(Event event) {
        User user = userService.getAuthenticatedUser();

        if (this.userService.isOwner(user, event)) {
            return "owner";
        }

        if (this.userService.isMember(user, event)) {
            return "member";
        }

        AuthUser authUser = userService.getAuthenticatedUserAuthUser();

        if (authUser != null) {
            return authUser.getEventId().equals(String.valueOf(event.getId())) ? "tmpUser" : "";
        }

        return "";
    }
}

