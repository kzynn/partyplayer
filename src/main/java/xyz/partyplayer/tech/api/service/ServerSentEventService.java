package xyz.partyplayer.tech.api.service;

import com.google.gson.Gson;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.stereotype.Service;
import reactor.core.publisher.DirectProcessor;
import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxProcessor;
import reactor.core.publisher.FluxSink;
import xyz.partyplayer.entity.persistence.Song;
import xyz.partyplayer.tech.api.model.PlayEvent;
import xyz.partyplayer.tech.api.model.SongResponse;
import xyz.partyplayer.tech.api.model.SseEvent;
import xyz.partyplayer.tech.api.model.VoteEvent;

import java.util.UUID;

@Service
public class ServerSentEventService {

    private final FluxProcessor processor = DirectProcessor.create().serialize();
    private final FluxSink<SseEvent> sink = processor.sink();

    public Flux<ServerSentEvent<String>> getSongEventsForEvent(UUID id) {
        return processor
                .filter(e -> ((SseEvent) e).getEvent().equals(id))
                .map(e -> ((SseEvent) e).getData())
                .map(e -> {
                    if (e instanceof VoteEvent) {
                        VoteEvent e2 = (VoteEvent) e;
                        return ServerSentEvent.builder(new Gson().toJson(e2)).event("vote-event").build();
                    }
                    if (e instanceof SongResponse) {
                        SongResponse e2 = (SongResponse) e;
                        return ServerSentEvent.builder(new Gson().toJson(e2)).event("add-song-event").build();
                    }
                    if (e instanceof PlayEvent) {
                        PlayEvent e2 = (PlayEvent) e;
                        return ServerSentEvent.builder(new Gson().toJson(e2)).event("play-song-event").build();
                    }

                    // This case should never be called
                    return "";
                });
    }

    public void createVoteEvent(Song song) {
        sink.next(new SseEvent(song.getEvent().getId(), new VoteEvent(song.getId().toString(), song.getVoteCount())));
    }

    public void createAddSongEvent(Song song, UUID event) {
        sink.next(new SseEvent(event, SongResponse.from(song)));
    }

    public void createPlaySongEvent(Song song) {
        sink.next(new SseEvent(song.getEvent().getId(), new PlayEvent(song.getId().toString())));
    }

}

