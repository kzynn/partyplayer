package xyz.partyplayer.tech.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.partyplayer.entity.persistence.User;
import xyz.partyplayer.knowledge.repository.SpotifyTokenRepository;
import xyz.partyplayer.tech.api.model.UserDataResponseCredentials;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class CredentialService {

    private final SpotifyTokenRepository tokenRepository;

    @Autowired
    public CredentialService(SpotifyTokenRepository tokenRepository) {
        this.tokenRepository = tokenRepository;
    }

    public List<UserDataResponseCredentials> getCredentials(User user) {
        List<UserDataResponseCredentials> credentialsList = new ArrayList<>();

        if (tokenRepository.existsByUser_id(user.getId())) {
            UserDataResponseCredentials credentials = new UserDataResponseCredentials();
            credentialsList.add(credentials.type("Spotify"));
        }

        return credentialsList;
    }

    public boolean deleteSpotifyToken(User user) {
        return tokenRepository.deleteByUser_id(user.getId()) == 1;
    }

}
