package xyz.partyplayer.tech.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.partyplayer.entity.persistence.User;
import xyz.partyplayer.knowledge.repository.UserRepository;
import xyz.partyplayer.tech.api.model.SpotifyWebToken;
import xyz.partyplayer.tech.exception.NotFoundException;
import xyz.partyplayer.usecases.IGetCredentials;
import xyz.partyplayer.usecases.IPlaySong;

import java.util.Optional;
import java.util.UUID;

@Service
public class SpotifyService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private IGetCredentials credentials;

    @Autowired
    private IPlaySong play;

    public SpotifyWebToken getPlayToken(UUID id) {
        try {
            Optional<User> user = this.userRepository.findById(id);
            if (!user.isPresent()) {
                throw new NotFoundException(0, "User not found");
            }
            String token = this.credentials.getWebToken(user.get());
            SpotifyWebToken res = new SpotifyWebToken();
            res.setSpotifyWebToken(token);

            return res;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void playSong(String trackId, String device) {
        User authenticatedUser = this.userService.getAuthenticatedUser();
        try {
            this.play.Play(authenticatedUser, device, trackId);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
