package xyz.partyplayer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import xyz.partyplayer.knowledge.externalApi.SpotifyApi;
import xyz.partyplayer.knowledge.repository.SpotifyTokenRepository;
import xyz.partyplayer.knowledge.repository.UserRepository;
import xyz.partyplayer.tech.api.service.UserService;
import xyz.partyplayer.tech.security.JwtTokenProvider;
import xyz.partyplayer.usecases.streaming.Spotify;

@Configuration
public class Config {

    @Value("${sportify.clientId}")
    private String clientId = "";

    @Value("${sportify.secret}")
    private String secret = "";

    @Value("${sportify.uri}")
    private String uri = "";

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository repo;

    @Autowired
    private SpotifyTokenRepository spotifyTokenRepository;


    @Bean
    public Spotify createIPlaySong() {
        SpotifyApi api = new SpotifyApi(this.clientId, this.secret, this.uri);
        Spotify spotify = new Spotify(api, this.userService, repo, spotifyTokenRepository);
        return spotify;
    }



/*

    @Bean
    public CommandLineRunner runner(UserRepository repository) {
        return (args -> {
            User user = new User();
            user.setUsername("root");
            user.setPassword("root");

            repository.save(user);

            /*
            user = repository.findByUsername("root");


            SpotifyToken token = new SpotifyToken();
            token.setUser_id(user.getId());
            token.setRefreshToken("AQBA_efe3hS-r26mnmt5l4E3C05l2ubktz7EF82V7lQJvA0x_jnK8QQOjsjr0zUqWcT54u1KyfAAe1FjjVq_cYMVoYRR2sPWOdyhaa6iLxG6-DPF1vRi1NG8cp6tDh1IJr0");

            token.setUser(user);
            user.setSpotifyToken(token);

            repository.save(user);
        });
    }
                */


    @Bean
    public JwtTokenProvider getProvider() {
        JwtTokenProvider jwtTokenProvider = new JwtTokenProvider();
        jwtTokenProvider.init();
        return jwtTokenProvider;
    }
}
