package xyz.partyplayer.entity.network;


public class SpotifyCreateAccessTokenResponse {
    private String error;

    public SpotifyCreateAccessTokenResponse(String code) {
        this.error = code;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
