package xyz.partyplayer.entity.network;


public class SpotifyCreateAccessTokenRequest {
    private String code;

    public SpotifyCreateAccessTokenRequest(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
