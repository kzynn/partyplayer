package xyz.partyplayer.entity.persistence;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

@Data
@NoArgsConstructor

@Entity
public class SpotifyToken implements Serializable {

    @Id
    @Column(name="user_id", length = 60)
    @Type(type="uuid-char")
    private UUID user_id;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToOne(optional = false, targetEntity = User.class)
    @PrimaryKeyJoinColumn(name="user_id", referencedColumnName="id")
    private User user;

    @Column(nullable = false)
    private String refreshToken;

    @Transient
    private String accessToken;

    @Transient
    private Date accessTokenExpire;

    public void setAccessTokenExpire() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MINUTE, 6);
        this.accessTokenExpire = new Date(c.getTimeInMillis());
    }

    @PreRemove
    private void preRemove() {
        user.setSpotifyToken(null);
    }
}
