package xyz.partyplayer.entity.persistence;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@EqualsAndHashCode

@Embeddable
public class VoteId implements Serializable {

    @Type(type="uuid-char")
    @Column(length = 60)
    private UUID songId;

    @Type(type="uuid-char")
    @Column(length = 60)
    private UUID userId;
}
