package xyz.partyplayer.entity.persistence;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Data
@NoArgsConstructor

@Entity
@Table(name = "votes")
public class Vote implements Serializable {

    @EmbeddedId
    private VoteId id;

    @ManyToOne(optional = false)
    @MapsId("songId")
    private Song song;

    @ManyToOne(optional = false)
    @MapsId("userId")
    private User user;

    @Column(nullable = false, updatable = false)
    private boolean isUpVote;

    public Vote(Song song, User user, boolean isUpVote) {
        this.id = new VoteId(song.getId(), user.getId());
        this.song = song;
        this.user = user;
        this.isUpVote = isUpVote;
    }

    public Vote(Song song, User user) {
        this(song, user, true);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj instanceof Vote) {
            Vote that = (Vote) obj;
            return that.getSong().getId().equals(this.song.getId()) && that.getUser().getId().equals(this.user.getId()) && that.isUpVote == this.isUpVote;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, song, user, isUpVote);
    }
}
