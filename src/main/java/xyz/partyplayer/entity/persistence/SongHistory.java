package xyz.partyplayer.entity.persistence;

import lombok.Data;
import org.hibernate.annotations.Type;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Data
@Entity
@EntityListeners(AuditingEntityListener.class)
public class SongHistory {
    @Id
    @Column(length = 60)
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Type(type="uuid-char")
    private UUID id;

    @Column(nullable = false)
    private String name;

    @Column()
    private String interpret;

    @Column()
    private int duration;

    @ManyToOne(optional = false, fetch = FetchType.LAZY, targetEntity = Event.class)
    private Event event;

    @Column(nullable = false)
    private String platform;

    @Column(nullable = false)
    private String playLink;

    @Column(nullable = false)
    private int votes;

    @Column()
    @Temporal(TemporalType.TIMESTAMP)
    private Date suggestedAt;

    @CreatedDate()
    @Temporal(TemporalType.TIMESTAMP)
    private Date playedAt;
}
