package xyz.partyplayer.entity.persistence;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.Type;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Data
@NoArgsConstructor

@Entity
@EntityListeners(AuditingEntityListener.class)
public class Song {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Type(type="uuid-char")
    @Column(length = 60)
    private UUID id;

    @Column(nullable = false)
    private String name;

    @Column()
    private String interpret;

    @Column()
    private int duration;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne(optional = false, fetch = FetchType.LAZY, targetEntity = Event.class)
    private Event event;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToOne(optional = false, fetch = FetchType.LAZY, targetEntity = User.class)
    private User suggestedBy;

    @Column(nullable = false)
    private String platform;

    @Column(nullable = false)
    private String playLink;

    @CreatedDate()
    @Temporal(TemporalType.TIMESTAMP)
    private Date suggestedAt;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "song", fetch = FetchType.LAZY, cascade = CascadeType.ALL, targetEntity = Vote.class)
    private Set<Vote> votes;

    public Song(String name, String interpret, Event event, User suggestedBy, String platform, String playLink, int duration) {
        this.name = name;
        this.interpret = interpret;
        this.event = event;
        this.suggestedBy = suggestedBy;
        this.platform = platform;
        this.playLink = playLink;
        this.duration = duration;
        this.votes = new HashSet<>();
    }

    public int getVoteCount() {
        // TODO: optimize by selecting voteCount with select directly
        return this.getVotes().stream()
                .map(vote -> vote.isUpVote() ? 1 : -1)
                .reduce(0, Integer::sum);
    }
}
