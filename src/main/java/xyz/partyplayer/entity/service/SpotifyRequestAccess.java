package xyz.partyplayer.entity.service;

public class SpotifyRequestAccess extends AbstractRequestAccess {
    private String code;

    public SpotifyRequestAccess(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
