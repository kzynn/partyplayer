package xyz.partyplayer.usecases;

import org.springframework.stereotype.Service;
import xyz.partyplayer.entity.persistence.Song;
import xyz.partyplayer.tech.api.model.SearchResult;

import java.util.List;

@Service
public interface ISearchSong {
    List<SearchResult> searchSongByTitle (String name) throws Exception;
    Song searchSongById (String id) throws Exception;
}
