package xyz.partyplayer.usecases;

import org.springframework.stereotype.Service;
import xyz.partyplayer.entity.persistence.User;

@Service
public interface IPlaySong {
    void Play(User user, String device, String trackId) throws Exception;
}
