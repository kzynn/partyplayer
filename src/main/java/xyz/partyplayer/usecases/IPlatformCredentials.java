package xyz.partyplayer.usecases;

import xyz.partyplayer.entity.service.AbstractRequestAccess;
import xyz.partyplayer.tech.exception.CreateCreditsException;

import java.util.InputMismatchException;

public interface IPlatformCredentials {
    void createCredits(AbstractRequestAccess o)
        throws InputMismatchException, CreateCreditsException;
}
