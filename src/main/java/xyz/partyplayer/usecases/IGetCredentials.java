package xyz.partyplayer.usecases;

import org.springframework.stereotype.Service;
import xyz.partyplayer.entity.persistence.User;
import xyz.partyplayer.tech.exception.CreateCreditsException;

@Service
public interface IGetCredentials {
    String getWebToken(User user) throws CreateCreditsException;
}
