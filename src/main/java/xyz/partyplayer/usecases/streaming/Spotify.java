package xyz.partyplayer.usecases.streaming;

import xyz.partyplayer.entity.externalApi.SpotifyExternalResponseAccessToken;
import xyz.partyplayer.entity.persistence.Song;
import xyz.partyplayer.entity.persistence.SpotifyToken;
import xyz.partyplayer.entity.persistence.User;
import xyz.partyplayer.entity.service.AbstractRequestAccess;
import xyz.partyplayer.entity.service.SpotifyRequestAccess;
import xyz.partyplayer.knowledge.externalApi.SpotifyApi;
import xyz.partyplayer.knowledge.repository.SpotifyTokenRepository;
import xyz.partyplayer.knowledge.repository.UserRepository;
import xyz.partyplayer.tech.api.model.SearchResult;
import xyz.partyplayer.tech.api.service.UserService;
import xyz.partyplayer.tech.exception.CreateCreditsException;
import xyz.partyplayer.usecases.IGetCredentials;
import xyz.partyplayer.usecases.IPlatformCredentials;
import xyz.partyplayer.usecases.IPlaySong;
import xyz.partyplayer.usecases.ISearchSong;

import java.util.Calendar;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.List;

public class Spotify implements IPlatformCredentials, ISearchSong, IGetCredentials, IPlaySong {

    private final UserService userService;
    private UserRepository repository;
    private SpotifyTokenRepository spotifyTokenRepository;
    private SpotifyApi api;

    public Spotify(SpotifyApi api, UserService userService, UserRepository repository, SpotifyTokenRepository spotifyTokenRepository) {
        this.api = api;
        this.userService = userService;
        this.repository = repository;
        this.spotifyTokenRepository = spotifyTokenRepository;
    }

    @Override
    public void createCredits(AbstractRequestAccess o) throws InputMismatchException, CreateCreditsException {
        if (!(o instanceof SpotifyRequestAccess)) {
            throw new InputMismatchException("Expected was SpotifyRequestAccess.class but got " + o.getClass().toString());
        }
        SpotifyRequestAccess requestAccess = (SpotifyRequestAccess) o;

        User authenticatedUser = userService.getAuthenticatedUser();

        SpotifyToken tocken = new SpotifyToken();
        tocken.setUser_id(authenticatedUser.getId());



        SpotifyExternalResponseAccessToken token = this.api.getAccessTokenAndRefreshToken(requestAccess.getCode());
        tocken.setAccessToken(token.getAccess_token());
        tocken.setAccessTokenExpire(this.calculateExpireToken(token.getExpires_in()));
        tocken.setRefreshToken(token.getRefresh_token());

        tocken.setUser(authenticatedUser);
        tocken.setUser_id(authenticatedUser.getId());
        authenticatedUser.setSpotifyToken(tocken);

        repository.save(authenticatedUser);
    }

    private Date calculateExpireToken(String offset){
        Calendar instance = Calendar.getInstance();
        instance.add(Calendar.SECOND, Integer.parseInt(offset));
        return instance.getTime();
    }


    @Override
    public List<SearchResult> searchSongByTitle(String name) throws Exception {
        User authenticatedUser = userService.getAuthenticatedUser();
        return api.searchSong(name, authenticatedUser.getSpotifyToken().getRefreshToken());

    }

    @Override
    public Song searchSongById(String id) throws Exception {
        User authenticatedUser = userService.getAuthenticatedUser();
        return api.searchById(id, authenticatedUser.getSpotifyToken().getRefreshToken());
    }

    @Override
    public String getWebToken(User user) throws CreateCreditsException {
        return api.refreshAccessToken(user.getSpotifyToken().getRefreshToken()).getAccess_token();
    }

    @Override
    public void Play(User user, String device, String trackId) throws Exception {
        this.api.playSong(user.getSpotifyToken().getRefreshToken(), device, trackId);
    }
}
