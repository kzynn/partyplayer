package xyz.partyplayer;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@EnableJpaAuditing
@ComponentScan(basePackages = {"xyz.partyplayer", "xyz.partyplayer.tech.api", "xyz.partyplayer.configuration", "xyz.partyplayer.tech.security", "xyz.partyplayer.entity", "xyz.partyplayer.knowledge.repository"})
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class})
public class PartyPlayerAPI implements CommandLineRunner {

    public static void main(String[] args) throws Exception {
        new SpringApplication(PartyPlayerAPI.class).run(args);
    }

    @Override
    public void run(String... arg0) throws Exception {
        if (arg0.length > 0 && arg0[0].equals("exitcode")) {
            throw new ExitException();
        }
    }

    class ExitException extends RuntimeException implements ExitCodeGenerator {
        private static final long serialVersionUID = 1L;

        @Override
        public int getExitCode() {
            return 10;
        }

    }
}
